import xml.etree.ElementTree as exml
import html
from collections import Counter

import utils as Utils
import wsd_tokenizer as tokenizer
from chronometer import Chrono
from wsd_constants import *
from domain_classifier import word2vec

DATASET_XML = {TRAIN: TRAIN_DATASET_WSD,
               DEV: DEV_DATASET_WSD,
               TEST: None}

BINDING_TXT = {TRAIN: TRAIN_BINDING_WSD,
               DEV: DEV_BINDING_WSD}

ID = "id"
POS = "pos"
LEMMA = "lemma"
WORD = "word"


class Loader:
    """
    Loads the data, and performs some preprocessing on it.
    Organizes the data in a easy-to-use way.
    """
    instance = None

    @staticmethod
    def get_instance(renew=False):
        """
        Singleton pattern, with cache and possibility to renew it
        :param renew: Whether to renew the cache
        :return: an instance of this class
        """

        if Loader.instance and not renew:
            return Loader.instance

        c = Chrono("Loading datasets...")
        cache = None
        if renew:
            Loader.instance = Loader()
        else:
            cache = Utils.load_pickle(CACHE_FILE_LOADER)
            Loader.instance = cache if cache else Loader()

        if not cache:
            c.millis("generated")
            Utils.save_pickle(Loader.instance, CACHE_FILE_LOADER, override=renew)
        else:
            c.millis("cached")
        return Loader.instance

    def __init__(self):
        self.doc_sentences = {x: {y: [] for y in [ID, POS, LEMMA, WORD]} for x in [TRAIN, DEV, TEST]}
        self.doc_sentences_word_restored = {x: [] for x in [TRAIN, DEV, TEST]}
        self.doc_sentences_tokenized = {x: [] for x in [TRAIN, DEV, TEST]}

        self.synset2id = {x: {} for x in [TRAIN, DEV]}
        self.id2synset = {x: {} for x in [TRAIN, DEV]}

        self.lemma2synsets = {x: {} for x in [TRAIN, DEV]}
        self.word2synsets = {x: {} for x in [TRAIN, DEV]}

        self.lemma_pos2synsets = {x: {} for x in [TRAIN, DEV]}
        self.word_pos2synsets = {x: {} for x in [TRAIN, DEV]}

        self._load_data()
        self._load_bindings()
        self._load_test_data()
        self._restore_sentences()
        self._tokenize_restored_sentences()
        self._determine_possible_synsets()

    def _determine_possible_synsets(self):
        """
        Saves in the correspondent map the possible synset for each word, lemma, (word,pos) and (lemma,pos)
        """
        for dataset_name in [TRAIN, DEV]:
            for d, document in enumerate(self.doc_sentences[dataset_name][ID]):
                for s, sentence in enumerate(document):
                    for i, id in enumerate(sentence):
                        if id is None:
                            continue

                        word = self.doc_sentences[dataset_name][WORD][d][s][i]
                        lemma = self.doc_sentences[dataset_name][LEMMA][d][s][i]
                        pos = self.doc_sentences[dataset_name][POS][d][s][i]
                        true_synset = self.id2synset[dataset_name][id]

                        if lemma not in self.lemma2synsets[dataset_name]:
                            self.lemma2synsets[dataset_name][lemma] = Counter()
                        self.lemma2synsets[dataset_name][lemma][true_synset] += 1

                        if word not in self.word2synsets[dataset_name]:
                            self.word2synsets[dataset_name][word] = Counter()
                        self.word2synsets[dataset_name][word][true_synset] += 1

                        lemmapos = (lemma, pos)
                        if lemmapos not in self.lemma_pos2synsets[dataset_name]:
                            self.lemma_pos2synsets[dataset_name][lemmapos] = Counter()
                        self.lemma_pos2synsets[dataset_name][lemmapos][true_synset] += 1

                        wordpos = (word, pos)
                        if wordpos not in self.word_pos2synsets[dataset_name]:
                            self.word_pos2synsets[dataset_name][wordpos] = Counter()
                        self.word_pos2synsets[dataset_name][wordpos][true_synset] += 1

    def _load_test_data(self):
        """
        Loads the test data, according to its format.
        """
        self._add_document(TEST)

        with open(TEST_DATASET_AND_BINDIGS_WSD) as f:
            for sentence in f.readlines():
                s = {x: [] for x in [ID, POS, LEMMA, WORD]}

                for word_token in sentence.split():
                    tokens = word_token.split("|")
                    s[WORD].append(tokens[0])
                    s[LEMMA].append(tokens[1])
                    s[POS].append(tokens[2])
                    s[ID].append(tokens[3] if len(tokens) == 4 else None)

                for key in s:
                    self.doc_sentences[TEST][key][0].append(s[key])

    def _load_data(self):
        """
        Loads the data TRAIN and TEST, from the xml format.
        :return:
        """
        for dataset_name in [TRAIN, DEV]:
            root = exml.parse(DATASET_XML[dataset_name]).getroot()  # type: exml.Element

            for doc_num, document in enumerate(root):
                self._add_document(dataset_name)
                for sentence in document:
                    self._add_sentence(dataset_name, doc_num, sentence)

    def _add_document(self, dataset_name):
        for key in self.doc_sentences[dataset_name]:
            self.doc_sentences[dataset_name][key].append([])

    def _add_sentence(self, dataset_name, doc_num, elements):
        sequence = {x: [] for x in [ID, POS, LEMMA, WORD]}

        for e in elements:
            sequence[WORD].append(e.text)
            for key in [POS, LEMMA, ID]:
                sequence[key].append(e.attrib[key] if key in e.attrib else None)

        for key in sequence:
            self.doc_sentences[dataset_name][key][doc_num].append(sequence[key])

    def _restore_sentences(self):
        """
        Tries to de-tokenize the sentence (did n't -> didn't), in order to use the google embeddings.
        """
        no_prior_space = ["."]
        no_prior_space_if_apos = ["VERB", "ADV", "PRT"]

        for dataset_name in self.doc_sentences:
            if dataset_name is None:
                continue

            for d, document in enumerate(self.doc_sentences[dataset_name][WORD]):
                doc = []
                for s, sentences in enumerate(document):
                    sentence = ""
                    for w, word in enumerate(sentences):
                        word = html.unescape(word.replace("&amp;amp;", "&amp;")).strip()
                        space = " "
                        if (not sentence or self.doc_sentences[dataset_name][POS][d][s][w] in no_prior_space or
                                (self.doc_sentences[dataset_name][POS][d][s][w] in no_prior_space_if_apos and "'" in word)):
                            space = ""

                        sentence += space + word
                    doc.append(sentence)
                self.doc_sentences_word_restored[dataset_name].append(doc)

    @staticmethod
    def _restore_sentences_no_doc(doc_sentences):
        """
        Tries to de-tokenize the sentence (did n't -> didn't), in order to use the google embeddings.
        """
        no_prior_space = ["."]
        no_prior_space_if_apos = ["VERB", "ADV", "PRT"]

        restored = {}
        for dataset_name in doc_sentences:
            if dataset_name is None:
                continue
            restored[dataset_name] = []

            # for d, document in enumerate(doc_sentences[dataset_name][WORD]):
            #     doc = []
            for s, sentences in enumerate(doc_sentences[dataset_name][WORD]):
                sentence = ""
                for w, word in enumerate(sentences):
                    word = html.unescape(word.replace("&amp;amp;", "&amp;")).strip()
                    space = " "
                    if (not sentence or doc_sentences[dataset_name][POS][s][w] in no_prior_space or
                            (doc_sentences[dataset_name][POS][s][w] in no_prior_space_if_apos and "'" in word)):
                        space = ""

                    sentence += space + word
                # doc.append(sentence)
                restored[dataset_name].append(sentence)

    def _tokenize_restored_sentences(self):
        """
        Tokenizes the restored sentences into tokens that are present in the google embeddings.
        """
        for dataset_name in self.doc_sentences_word_restored:
            for d, document in enumerate(self.doc_sentences_word_restored[dataset_name]):
                doc = []
                for sentence in document:
                    tokenized = tokenizer.tokenize_line_google(sentence, word2vec.get())
                    doc.append(tokenized)
                self.doc_sentences_tokenized[dataset_name].append(doc)

    def _load_bindings(self):
        """
        Loads the ground truth for the TRAIN and TEST set
        """
        for dataset_name in [TRAIN, DEV]:
            with open(BINDING_TXT[dataset_name]) as f:
                for line in f.readlines():
                    splitted_line = line.split()
                    self.id2synset[dataset_name][splitted_line[0]] = splitted_line[1]
                    self.synset2id[dataset_name][splitted_line[1]] = splitted_line[0]

    @staticmethod
    def get_semcor(dataset_name, id):
        assert dataset_name == DEV
        return id.split(".")[0]

    @staticmethod
    def get_docid(dataset_name, id):
        s = id.split(".")
        if dataset_name == DEV:
            return "{}.{}".format(s[0], s[1])
        elif dataset_name == TRAIN:
            return "{}".format(s[0])

    @staticmethod
    def get_sentenceid(dataset_name, id):
        s = id.split(".")
        if dataset_name == DEV:
            return "{}.{}.{}".format(s[0], s[1], s[2])
        elif dataset_name == TRAIN:
            return "{}.{}".format(s[0], s[1])


if __name__ == '__main__':
    """
    Prints some statistics about the data.
    """
    a = Loader.get_instance()
    syntrain = set(a.id2synset[TRAIN].values())
    syndev = set(a.id2synset[DEV].values())
    unk =sum(1 for x in syndev if x not in syntrain)
    unknum = (sum([1 for b in a.id2synset[DEV].values() if b not in syntrain]))
    totdev = len(a.id2synset[DEV])
    print("Number of synset in the train dataset", len(syntrain))
    print("Number of synset in the dev dataset", len(syndev))
    print("Number of synset in the dev dataset that do not occur in the train", unk)
    print("Number of times the unk synsets"
          " occur in the dev", unknum)
    print()
    print("So, we can hit {}-{}={}  predictions on the dev, with an accuracy of: {}".format(
        totdev, unknum, totdev-unknum, 100 - (unknum / len(a.id2synset[DEV]) * 100 )
    ))
    print()


    not_word_but_synset = 0
    not_word_but_synset_unique = 0
    not_word = 0
    not_word_occ = 0
    for word, synstes in a.word2synsets[DEV].items():
        if word not in a.word2synsets[TRAIN]:
            not_word += 1
            for s in synstes:
                not_word_occ += synstes[s]
                if s in syntrain:
                    not_word_but_synset += synstes[s]
            for s in synstes:
                if s in syntrain:
                    not_word_but_synset_unique += 1
    print("There are {} words that do not appear in TRAIN, and they occur {} times".format(not_word, not_word_occ))
    print("There are {} words that do not apprea in TRAIN but have a synset in it, the they occur {} times".format(not_word_but_synset_unique, not_word_but_synset))

    print()
    not_lemma_but_synset = 0
    not_lemma_but_synset_unique = 0
    not_lemma = 0
    not_lemma_occ = 0
    for lemma, synstes in a.lemma2synsets[DEV].items():
        if lemma not in a.lemma2synsets[TRAIN]:
            not_lemma += 1
            for s in synstes:
                not_lemma_occ += synstes[s]
                if s in syntrain:
                    not_lemma_but_synset += synstes[s]
            for s in synstes:
                if s in syntrain:
                    not_lemma_but_synset_unique += 1
    print("There are {} lemmas that do not appear in TRAIN, and they occur {} times".format(not_lemma, not_lemma_occ))
    print("There are {} lemmas that do not apprea in TRAIN but have a synset in it, the they occur {} times".format(
        not_lemma_but_synset_unique, not_lemma_but_synset))

    print()
    for x in [DEV, TRAIN]:
        print("There are {} unique lemmas in {}, they appear {} times".format(len(a.lemma2synsets[x]), x, sum(a.lemma2synsets[x][y][z] for y in a.lemma2synsets[x] for z in a.lemma2synsets[x][y])))
        print("There are {} unique words in {},  they appear {} times".format(len(a.word2synsets[x]), x, sum(a.word2synsets[x][y][z] for y in a.word2synsets[x] for z in a.word2synsets[x][y])))
        print()

    for x in [DEV, TRAIN]:
        print("There are {} unique lemmas-pos in {}, they appear {} times".format(len(a.lemma_pos2synsets[x]), x, sum(
            a.lemma_pos2synsets[x][y][z] for y in a.lemma_pos2synsets[x] for z in a.lemma_pos2synsets[x][y])))
        print("There are {} unique words-pos in {},  they appear {} times".format(len(a.word_pos2synsets[x]), x, sum(
            a.word_pos2synsets[x][y][z] for y in a.word_pos2synsets[x] for z in a.word_pos2synsets[x][y])))
        print()

    for x in [TRAIN, DEV, TEST]:
        print("{} number of original sentences: {}".format(x, sum(len(y) for y in a.doc_sentences[x][WORD])))
    print()

    for x in [TRAIN, DEV, TEST]:
        print("{} number of tokenized sentences: {}".format(x, sum(len(y) for y in a.doc_sentences_tokenized[x])))

    print()

    for x in [TRAIN, DEV]:
        print("{} to disambiguate: {}".format(x, sum(a.word2synsets[x][z][y] for z in a.word2synsets[x] for y in a.word2synsets[x][z] )))

        print("{} number of instance words to disambiguate {}".format(x, sum(a.word2synsets[x][z][y] for z in a.word2synsets[x]
                                                                    for y in a.word2synsets[x][z] if z in a.word2synsets[TRAIN] and len(a.word2synsets[TRAIN][z])>1)))
        print(x, " ids to disambiguate:", len(a.id2synset[TRAIN]))
    # strange = {x: set() for x in [TRAIN, DEV]}
    bula =0
    for x in [ DEV]:
        for d, doc in enumerate(a.doc_sentences_tokenized[x]):
            for s, sentence in enumerate(doc):
                bula += 1
    print(bula)
