import utils
from chronometer import Chrono
from srl_constants import *


class Loader:
    """
    Manages the loading and the storing modalities of the raw data.
    It uses a cached singleton pattern.
    """
    instance = None

    @staticmethod
    def get_instance(renew=False):
        """
        Singleton pattern, with cache and possibility to renew it
        :param renew: Whether to renew the cache
        :return: an instance of this class
        """

        if Loader.instance and not renew:
            return Loader.instance

        c = Chrono("Loading loader...")
        cache = None
        if renew:
            Loader.instance = Loader()
        else:
            cache = utils.load_pickle(CACHE_LOADER)
            Loader.instance = cache if cache else Loader()

        if not cache:
            c.millis("generated")
            utils.save_pickle(Loader.instance, CACHE_LOADER, override=renew)
        else:
            c.millis("cached")
        return Loader.instance

    def __init__(self):
        self.data = {dataset: {col: [] for col in COLUMNS} for dataset in DATASETS}

        self._load_data()

    @staticmethod
    def _decode_value(value):
        """
        Transforms the indicator for a value not present from _ to None

        :param value: the value to be transformed
        :return: the transformed value
        """
        return None if value == "_" else value

    @staticmethod
    def _decode_line(line):
        """
        Splits the line into tokens, the last token can have a variable length.
        The number of the initial tokens is fixed

        :param line: the line to be splitted
        :return: N tokens, the last token is a list.
        """
        if not line.strip():
            return None
        splits = line.split()
        return (*splits[:len(FIXED_COLUMNS)], splits[len(FIXED_COLUMNS):])

    @staticmethod
    def _add_word_to_sentence(sentence, line):
        """
        Associates a decoded token to its description.
        The list of headers is in defined in COLUMNS
        :param sentence: the sentence to which the line must be added.
        :param line: the list of tokens
        :return: True if succeded
        """
        decoded = Loader._decode_line(line)
        if not decoded:
            return False

        for key, value in zip(COLUMNS, decoded):
            sentence[key].append(Loader._decode_value(value))
        return True

    def _add_sentence(self, dataset, sentence):
        """
        Adds a sentence to the global variable that stores all the data.

        :param dataset: the dataset
        :param sentence: the sentence to store
        """
        for key, value in sentence.items():
            self.data[dataset][key].append(value)

    def _load_data(self):
        """
        Generates the global dictionary self.data, to store all the data.
        """

        for dataset in DATASETS:
            with open(DATASETS_FILE[dataset], "r") as file:
                sentence = {col: [] for col in COLUMNS}
                for line in file.readlines():

                    result = Loader._add_word_to_sentence(sentence, line)
                    if not result:
                        self._add_sentence(dataset, sentence)
                        sentence = {col: [] for col in COLUMNS}


if __name__ == '__main__':
    b = Loader.get_instance(True)
    print()

    print("Number of sentences")
    for x in DATASETS:
        print("\t{}:\t{}".format(x, len(b.data[x][ID])))
    print()

    print("Sample data")
    for x in COLUMNS:
        print("\t{}\t{}".format(x.ljust(8), b.data[TRAIN][x][:10]))
    print()
