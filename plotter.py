import os
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib
import matplotlib.pyplot as plt


PICS_FOLDER = "pics/"

params = {'legend.fontsize': 'x-large',
          'figure.figsize': (15, 5),
         'axes.labelsize': 'x-large',
         'axes.titlesize':'x-large',
         'xtick.labelsize':'x-large',
         'ytick.labelsize':'x-large'}
plt.rcParams.update(params)

CMAP = sns.light_palette((210, 90, 60), n_colors=64, input="husl")


def plot_confusion_matrix(labels, confusion_matrix):
    """
    Plots the confusion matrix using a seaborn heatmap
    :param labels: labels of the confusion matrix
    :param confusion_matrix: the confusion of matrix, in a list of list format
    """
    sns.set(style="white")
    data = pd.DataFrame(confusion_matrix, index=[i for i in range(len(labels))], columns=[i for i in range(len(labels))])

    f, ax = plt.subplots(figsize=(20,20))
    annot = data
    data = np.where(data != 0, np.log(data), 0)
    heatmap = sns.heatmap(data, xticklabels=labels, cbar=False, yticklabels=labels, cmap=CMAP,
                square=True, annot=annot, fmt="g", linewidths=1, annot_kws={"size": 11})  # font size
    heatmap.yaxis.set_ticklabels(heatmap.yaxis.get_ticklabels(), rotation=0, ha='right', fontsize=12)
    heatmap.xaxis.set_ticklabels(heatmap.xaxis.get_ticklabels(), rotation=45, ha='right', fontsize=12)
    plt.title("CONFUSION MATRIX")
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.tight_layout()
    plt.savefig(os.path.join(PICS_FOLDER, "confusion_matrix.png"), dpi=500)
    plt.close(f)
