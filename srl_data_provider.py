import numpy as np

import utils

from conll_disambiguator import Disambiguator
from srl_embeddings_loader import Embeds
from srl_data_loader import Loader
from chronometer import Chrono
from srl_constants import *


class Provider:
    """
    It takes the data stored in python structures, and provides it when needed in a useful form.
    It uses a cached singleton pattern.
    """

    instance = None

    @staticmethod
    def get_instance(renew=False):
        """
        Singleton pattern, with cache and possibility to renew it
        :param renew: Whether to renew the cache
        :return: an instance of this class
        """

        if Provider.instance and not renew:
            return Provider.instance

        c = Chrono("Loading provider...")
        cache = None
        if renew:
            Provider.instance = Provider()
        else:
            cache = utils.load_pickle(CACHE_PROVIDER)
            Provider.instance = cache if cache else Provider()

        if not cache:
            c.millis("generated")
            utils.save_pickle(Provider.instance, CACHE_PROVIDER, override=renew)
        else:
            c.millis("cached")
        return Provider.instance

    def __init__(self):
        self.loader = Loader.get_instance()
        self.disambiguator = Disambiguator.get_instance()

        self.dataset_dictionary = {dataset: {col: {} for col in COLUMNS} for dataset in DATASETS}
        self.dataset_reverse_dictionary = {dataset: {col: {} for col in COLUMNS} for dataset in DATASETS}

        self.global_dictionary = {col: {} for col in COLUMNS}
        self.global_reverse_dictionary = {col: {} for col in COLUMNS}

        self.word_dictionary = {}
        self.reverse_word_dictionary = {}

        self.word_and_synset_dictionary = {}
        self.reverse_word_and_synset_dictionary = {}

        self.embeddings_used = None
        self.predicate_positions = []
        self.sentences_len = []

        self._generate_dictionaries()
        self._get_used_embeddings()
        self._extract_predicate_positions()
        self._compute_sentence_len()

    def decode_predictions(self, preds):
        """
        Decodes a list of numeric representation of the predictions
        :param preds: encoded predictions
        :return: decoded predictions
        """
        return [self.global_reverse_dictionary[APREDS][x] for x in preds]

    @staticmethod
    def decode_representation(x):
        """
        Decodes the representation of the values (_ is equal to None)
        :param x: a value
        :return: the decoded value
        """
        return x if x is not None else "_"

    def _add_token(self, dataset, col, value):
        """
        Generates the dictionary, and its reverse, from a token to its codified number.
        Both the global ones, and per-dataset.
        :param dataset:  the current dataset
        :param col: the current col
        :param value: the token
        """
        if value not in self.dataset_dictionary[dataset][col]:
            index = len(self.dataset_dictionary[dataset][col])
            self.dataset_dictionary[dataset][col][value] = index
            self.dataset_reverse_dictionary[dataset][col][index] = value

        if value not in self.global_dictionary[col]:
            index = len(self.global_dictionary[col])
            self.global_dictionary[col][value] = index
            self.global_reverse_dictionary[col][index] = value

    def _add_token_word(self, value):
        """
        Adds the mapping, unifying the dictionaries: FORM, LEMMA and PLEMMA

        :param value: the word to map into a number
        """
        if value in self.word_dictionary:
            return None

        index = len(self.word_dictionary)
        self.word_dictionary[value] = index
        self.reverse_word_dictionary[index] = value

    def _add_token_synset(self, value):
        """
        Adds the mapping, unifying the words dictionary and the synsets

        :param value:  the token to map into a number
        :return:
        """
        if value in self.word_and_synset_dictionary:
            return None

        index = len(self.word_and_synset_dictionary)
        self.word_and_synset_dictionary[value] = index
        self.reverse_word_and_synset_dictionary[index] = value

    def _generate_dictionaries(self):
        """
        Generates the dictionaries that maps a given token into a number and viceversa
        """
        for i_dat, datset in enumerate(DATASETS):
            for column in COLUMNS:
                for i_sen, sentence in enumerate(self.loader.data[datset][column]):
                    for token in sentence:
                        if column == APREDS:
                            for subtoken in token:
                                self._add_token(datset, column, subtoken)
                        elif column == ID:
                            self._add_token(datset, column, (i_dat, i_sen, token))
                        else:
                            self._add_token(datset, column, token)

                        if column == FORM or column == LEMMA or column == PLEMMA:
                            self._add_token_word(token)

        self.word_and_synset_dictionary = dict(self.word_dictionary)
        self.reverse_word_and_synset_dictionary = dict(self.reverse_word_dictionary)

        for dataset in DATASETS:
            for sentence in self.disambiguator.disambiguated_conll[dataset]:
                for synset in sentence:
                    if synset:
                        self._add_token_synset(synset)

    def _compute_sentence_len(self):
        """
        Computes and stores the length of each sentence, for each dataset
        """
        for i_dataset, dataset in enumerate(DATASETS):
            lens = []
            for i_sentence, sentence in enumerate(self.loader.data[dataset][ID]):
                lens.append(len(sentence))
            self.sentences_len.append(lens)  # the index determines the dataset which it's referring to

    def _extract_predicate_positions(self):
        """
        For each dataset and sentence, extract the position of the predicates to speedup the lookup.
        Moreover, memorize the rank of the predicate in the sentence.
        """
        for i_dataset, dataset in enumerate(DATASETS):
            positions = []
            for i_sentence, sentence in enumerate(self.loader.data[dataset][FILLPRED]):
                pred_number_in_sentence = 0
                for i_token, token in enumerate(self.loader.data[dataset][FILLPRED][i_sentence]):
                    if token:
                        positions.append((i_dataset, i_sentence, i_token, pred_number_in_sentence))
                        pred_number_in_sentence += 1
            self.predicate_positions.append(positions)

    def _get_used_embeddings(self):
        """
        Looks up the embeddings of the used words and memorizes those embeddings.

        If the embedding of a word is not present,
        it's used the average embedding of all the used and valid embeddings.
        """
        embds = Embeds.get_instance()

        words_sorted = sorted(self.word_and_synset_dictionary.items(), key=lambda x: x[1])

        embeddings_used = [embds[x] if x in embds else None for x, _ in words_sorted]

        # skip none to get centroid
        avg_emb = np.mean([x for x in embeddings_used if x is not None], axis=0)

        self.embeddings_used = [x if x is not None else avg_emb for x in embeddings_used]

        # print("Number of valid embeddings found (FORM, LEMMA, PLEMMA)", len(valid_embeddings))
        # print("Total number of embeddings, filling with average:", len(self.embeddings_used))


if __name__ == '__main__':
    b = Provider.get_instance()

    # todo: consistency checks
    for x in b.word_dictionary:
        assert x in b.word_and_synset_dictionary and b.word_dictionary[x] == b.word_and_synset_dictionary[x]

    for x in b.reverse_word_dictionary:
        assert x in b.reverse_word_and_synset_dictionary and b.reverse_word_dictionary[x] == \
               b.reverse_word_and_synset_dictionary[x]

    assert len(b.word_dictionary) == len(b.reverse_word_dictionary)
    assert len(b.word_and_synset_dictionary) == len(b.reverse_word_and_synset_dictionary)
    assert len(b.word_dictionary) < len(b.word_and_synset_dictionary)

    assert len(b.word_and_synset_dictionary) == len(b.embeddings_used)

    print("Number of distinct words")
    for x in DATASETS:
        print("\t{}:\t{}".format(x, len(b.dataset_dictionary[x][FORM])))
    print()

    print("Sample dictionaries")
    for x in COLUMNS:
        print("\t{}\t{}".format(x.ljust(8), list(b.dataset_dictionary[TRAIN][x].items())[:100]))
    print()
