import os
import math
import random
from collections import Counter

import gensim
import numpy
import json
import time
import tqdm

import scipy.spatial
import sklearn.metrics
import sklearn.ensemble
import sklearn.svm
import xgboost

import utils as Utils
import wsd_tokenizer as tokenizer
from chronometer import Chrono
from RunInformation import RunInfo
from wsd_constants import *

import warnings
warnings.filterwarnings(action='ignore', category=DeprecationWarning)


######## KEYS

# training dictionary keys
X_TRAIN = "x_train"
Y_TRAIN = "y_train"
X_DEV = "x_dev"
Y_DEV = "y_dev"

TRAIN_FILES_IDS = "train_files_ids"
DEV_FILES_IDS = "dev_files_ids"

TRAIN_FILENAMES = "train_filename_idfile"
DEV_FILENAMES = "dev_filename_idfile"

TRAIN_IDFILE_DOMAIN = "train_idfile_domain"
DEV_IDFILE_DOMAIN = "dev_idfile_domain"


class word2vec:
    instance = None

    @staticmethod
    def get():
        if not word2vec.instance:
            word2vec.instance = word2vec()
        return word2vec.instance.embeddings

    def __init__(self):
        c = Chrono("Loading embeddings...")
        # Load Google's pre-trained Word2Vec model.
        self.embeddings = gensim.models.KeyedVectors.load_word2vec_format(GOOGLE_EMBEDDINGS, binary=True)
        c.millis()

class DataManager:
    """
    Manages the loading of the dataset, the conversion with the embeddings and performs some statistics.
    """

    @staticmethod
    def get_run_information() -> 'RunInfo':
        c = Chrono("Loading run information...")
        run_info = Utils.load_pickle(RUN_INFORMATION_FILE, folder=".")
        if not run_info:
            dm = DataManager.get_instance()
            run_info = dm.run_information
        c.millis()
        return run_info

    @staticmethod
    def get_instance(train_dir=TRAIN_DIR_CLASSIFIER, dev_dir=DEV_DIR_CLASSIFIER, test_dir=TEST_DIR_CLASSIFIER) -> 'DataManager':
        """
        Loads an instance of the class from a pickle file, if there is a cached version.

        :param train_dir: the train dir
        :param dev_dir: the dev dir
        :param test_dir: the test dir
        :return: an istance of Data Manager
        """
        c = Chrono("Loading the data...")
        instance = Utils.load_pickle(DATAMANAGER_CACHE_FILE, folder=CACHE_FOLDER_CLASSIFIER)
        if not instance:
            instance = DataManager(train_dir, dev_dir, test_dir)
            Utils.save_pickle(instance, DATAMANAGER_CACHE_FILE, folder=CACHE_FOLDER_CLASSIFIER)
            print("Datamanager saved")
            Utils.save_pickle(instance.run_information, RUN_INFORMATION_FILE, folder=".", override=True)
            print("Run info saved")
        c.millis()
        return instance

    def __init__(self, train_dir=TRAIN_DIR_CLASSIFIER, dev_dir=DEV_DIR_CLASSIFIER, test_dir=TEST_DIR_CLASSIFIER):
        self.test_dir = test_dir
        self.dev_dir = dev_dir
        self.train_dir = train_dir

        self.name_domain2token_domain = {}
        self.list_domains = None

        # ----- EACH DICTIONARY IS DIVIDED FIRSTLY BY TRAIN, DEV OR TEST ----- #
        # self.files_sentences_words = {}
        self.data_loader = {TRAIN: self.load_train_data, DEV: self.load_dev_data}

        self.id_file2domain = {}

        self.files_ids = {}
        self.files_names = {}

        self.id_file2setwords = {}
        self.id_file2word2number_words = {}

        # self.word2total_count = {}
        self.domain2word2total_count = {}
        self.word2doc_frequency = {}

        # -------------------------------------------------------------------- #
        self.load_train_data()
        self.load_dev_data()
        # self.load_test_data()
        self.count_stats()

        self.run_information = RunInfo(self.list_domains, self.name_domain2token_domain, self.word2doc_frequency, self.domain2word2total_count, {x: len(self.files_names[x]) for x in [TRAIN, DEV]})
        print("Creation complete")

    def count_stats(self):
        """
        Performs some statistics on the data, subdividing it based on TRAIN, TEST, DEV.
        It computes the:
        ) For each file, the number of occurrence of each word
        ) For each file, the set of words that appear in it
        ) For each word, the total number of occurrences
        ) For each word, its document frequency (number of documents in which it appears)
        """

        for dataset in [TRAIN, DEV]:
            self.id_file2setwords[dataset] = {}
            self.id_file2word2number_words[dataset] = {}
            # self.word2total_count[dataset] = Counter()
            self.word2doc_frequency[dataset] = Counter()

            bar = tqdm.tqdm(enumerate(self.data_loader[dataset]()[0]))
            for i, file in bar:
                bar.set_description("Counting for dataset: {}".format(dataset))
                file_set_words = set()
                word2count_in_file = Counter()

                for sentence in file:
                    file_set_words.update(sentence)
                    for word in sentence:
                        # if word in self.word2vec:
                        word2count_in_file[word] += 1
                        # self.word2total_count[dataset][word] += 1

                self.id_file2setwords[dataset][i] = file_set_words
                self.id_file2word2number_words[dataset][i] = word2count_in_file

                for word in file_set_words:
                    self.word2doc_frequency[dataset][word] += 1

        self.domain2word2total_count = {}
        for dataset in [TRAIN, DEV]:
            self.domain2word2total_count[dataset] = {}

            for file in self.files_ids[dataset]:
                if self.id_file2domain[dataset][file] not in self.domain2word2total_count[dataset]:
                    self.domain2word2total_count[dataset][self.id_file2domain[dataset][file]] = Counter()

                for word in self.id_file2setwords[dataset][file]:
                    self.domain2word2total_count[dataset][self.id_file2domain[dataset][file]][word] += self.id_file2word2number_words[dataset][file][word]

    def load_train_data(self):
        """
        Reads the train data
        """
        c = Chrono("Loading train data cache...")
        cache = Utils.load_pickle(DATAMANAGER_TRAIN_DATA, folder=CACHE_FOLDER_CLASSIFIER)
        if cache:
            _, self.list_domains, self.id_file2domain[TRAIN], self.files_ids[TRAIN], self.files_names[TRAIN] = cache

            for domain in self.list_domains:
                self.name_domain2token_domain[domain] = tokenizer.tokenize(domain, pattern="[\W_]")
            self.name_domain2token_domain["GAMES_AND_VIDEO_GAMES"] = ["games", "videogames"]  # manually corrected
            self.name_domain2token_domain['ART_ARCHITECTURE_AND_ARCHAEOLOGY'] = ["art", "architecture", "archeology"]
            self.name_domain2token_domain["LITERATURE_AND_THEATRE"] = ["literature", "theater"]
            print(self.name_domain2token_domain)
            return cache

        c.millis()

        c = Chrono("Loading train data...")
        to_unpack = self._load_data(self.train_dir)
        c.millis()

        c = Chrono("Saving train data...")
        Utils.save_pickle(to_unpack, DATAMANAGER_TRAIN_DATA, folder=CACHE_FOLDER_CLASSIFIER)
        c.millis()

        _, self.list_domains, self.id_file2domain[TRAIN], self.files_ids[TRAIN], self.files_names[TRAIN] = to_unpack

        for domain in self.list_domains:
            self.name_domain2token_domain[domain] = tokenizer.tokenize(domain, pattern="[\W_]")
        self.name_domain2token_domain["GAMES_AND_VIDEO_GAMES"] = ["games", "videogames"] # manually corrected
        self.name_domain2token_domain['ART_ARCHITECTURE_AND_ARCHAEOLOGY'] = ["art", "architecture", "archeology"]
        self.name_domain2token_domain["LITERATURE_AND_THEATRE"] = ["literature", "theater"]
        print(self.name_domain2token_domain)

        return to_unpack

    def load_dev_data(self):
        """
        Reads the dev data
        """

        c = Chrono("Loading dev data cache...")
        cache = Utils.load_pickle(DATAMANAGER_DEV_DATA, folder=CACHE_FOLDER_CLASSIFIER)
        if cache:
            _, _, self.id_file2domain[DEV], self.files_ids[DEV], self.files_names[DEV] = cache
            return cache
        c.millis()

        c = Chrono("Loading dev data...")
        to_unpack = self._load_data(self.dev_dir)
        c.millis()

        c = Chrono("Saving dev data...")
        Utils.save_pickle(to_unpack, DATAMANAGER_DEV_DATA, folder=CACHE_FOLDER_CLASSIFIER)
        c.millis()

        _, _, self.id_file2domain[DEV], self.files_ids[DEV], self.files_names[DEV] = to_unpack
        return to_unpack


    @staticmethod
    def _load_data(directory):
        """
        Read that data from directory, remembering the file name and the domain to which it belongs to.
        It generates a cache file for fast reuse.

        :param directory: the data directory

        :returns data: a list (files) of list (sentences) of words
        :returns list_domains: a list of domains encountered
        :returns idfile2domain: a dictionary that maps files to domani
        :returns file_ids: a list of file ids
        :returns file_names: a list of file names
        """

        data = []
        file_ids = []
        file_names = []

        id_file2domain = {}
        list_domains = []
        file_id = 0

        for domain in os.listdir(directory):
            list_domains.append(domain)

            listdir = os.listdir(os.path.join(directory, domain))
            for f in tqdm.tqdm(listdir, total=len(listdir)):
                if f.endswith(".txt"):
                    lines = DataManager._parse_file(os.path.join(directory, domain, f))
                    if lines:  # let out empty files, or files composed only by special chars!
                        data.append(lines)

                        file_ids.append(file_id)
                        file_names.append(f)

                        id_file2domain[file_id] = domain
                        file_id += 1

                    else:
                        print("\nWARNING: empty file ", f, " in ", directory, domain, "\n")
        return data, sorted(list_domains), id_file2domain, file_ids, file_names

    @staticmethod
    def _parse_file(filename):
        """
        Reads a file and parse it into a list of sentences

        :param filename: the file to read
        :return: a list of lines
        """
        lines = []
        with open(filename) as file:
            for line in file.readlines():

                splitted_line = tokenizer.tokenize_line_google(line, word2vec.get())
                if splitted_line:  # let out empty lines!!!
                    lines.append(splitted_line)
        return lines


class Metrics:
    """
    Manages the use of the embeddings and performs metric calculations on them
    """

    def __init__(self, load_only_run_information=True, compute_files_vectors=False):
        # self.run_info = DataManager.get_run_information()
        self.run_info = DataManager.get_run_information()
        if not load_only_run_information:
            self.dm = DataManager.get_instance()
            del self.dm.id_file2setwords

        # ------------  THE THRESHOLD ------------ #
        # Which words should I consider similar?
        # I consider similar words that are as distant at least as the words "crime" and "victim"
        self.reference_cos_sim = Metrics.cos_sim(word2vec.get()["history"], word2vec.get()["Napoleon"])

        # ----- EACH DICTIONARY IS DIVIDED FIRSTLY BY TRAIN, DEV OR TEST ----- #
        self.id_file2vec = {}
        self.id_file2embedding = {}
        self.id_file2domain2domain_importance = {}

        # ----> Computes the domain embedding!
        # At each domain is associated an embedding, considering the domain name as a list of word and taking the centroid
        self.domain2embedding = {
            domain: numpy.average([self.lookup(t) for t in self.run_info.name_domain2token_domain[domain] if self.lookup(t) is not None], axis=0) for
            domain in self.run_info.list_domains}
        # -------------------------------------------------------------------- #

        if compute_files_vectors:
            self.compute_files_embedding()
            self.compute_files_domain_importance()
            self.compute_file2vec()

        self.caching_importance = {}

    def lookup(self, word):
        """
        Return the embedding of a given word

        :param word: the given word
        :return: the embedding of the given word
        """
        # assert word in self.word2vec, word
        if word not in word2vec.get():
            print("WARNING: word not present in the dictionary ", word)
            return None
        return word2vec.get()[word] #if word in self.word2vec else self.word2vec["UNK"] #todo torna vettore medio

    @staticmethod
    def cos_sim(vec1, vec2):
        """
        Compute the cosine similarity (not efficient for large computations)

        :param vec1: the first 1-dimensional vector
        :param vec2: the second 1-dimensional vector
        :return: the computed cosine similarity
        """
        return 1 - scipy.spatial.distance.cosine(vec1, vec2)

    def compute_file2vec(self):
        """
        For each file, it computes the correspondent vector.
        For test purposes at each file is associated:
        ) The centroid of all the words embeddings in it
        ) The concatenation of the importance vector (see compute_files_domain_importance) and similarity vector,
            that is, the cosin similarity between the centroid of the file and the centroid of the domains name
        ) The concatenation of the previous two.
        """
        c = Chrono("Computing files features vectors...")

        cache = Utils.load_pickle(FILE_FEATURES_VECTORS_CACHE, folder=CACHE_FOLDER_CLASSIFIER)
        if cache:
            c.millis("loaded cached")
            self.id_file2embedding = cache
            return

        for dataset in [TRAIN, DEV]:
            self.id_file2vec[dataset] = []

            for file in tqdm.tqdm(range(len(self.dm.files_names[dataset]))):
                embed = self.id_file2embedding[dataset][file]
                imp = [self.id_file2domain2domain_importance[dataset][file][domain] for domain in self.dm.list_domains]
                cos = [self.cos_sim(self.id_file2embedding[dataset][file], self.domain2embedding[domain]) for domain in
                       self.dm.list_domains]
                self.id_file2vec[dataset].append(numpy.concatenate((embed, imp, cos)))

        Utils.save_pickle(self.id_file2embedding, FILE_FEATURES_VECTORS_CACHE, folder=CACHE_FOLDER_CLASSIFIER)
        c.millis()

    def compute_files_embedding(self):
        """
        For each file, computes the centroid of all the words in it.
        If a file is empty, it is considered equal to the embedding of UNK
        """
        c = Chrono("Computing file embeddings...")

        cache = Utils.load_pickle(FILE_EMBEDDINGS_CACHE, folder=CACHE_FOLDER_CLASSIFIER)
        if cache:
            c.millis("loaded cached")
            self.id_file2embedding = cache
            return

        for dataset in [DEV, TRAIN]:
            self.id_file2embedding[dataset] = {}
            sentences = self.dm.data_loader[dataset]()[0]
            for i, file in tqdm.tqdm(enumerate(sentences)):
                a = [self.lookup(word) for sentence in file for word in sentence]
                self.id_file2embedding[dataset][i] = numpy.average(a, axis=0)  # if a else self.dm.word2vec["UNK"] #todo centroide
            del sentences
        # del self.dm.files_sentences_words

        Utils.save_pickle(self.id_file2embedding, FILE_EMBEDDINGS_CACHE, folder=CACHE_FOLDER_CLASSIFIER)
        c.millis()

    def compute_files_domain_importance(self):
        """
        Computes a value "importance" for each pair file-domain.
        It is computed as follows:

        (remember that in the __init__ we calculated the embedding for each domain name)

        Given a file f, and the current domain d, the importance of the domain d for the file f is given by the formula:

            sum_{word in f | the similarity between the word and embedding(d) is at least the threshold}
                    {
                        similarity(word, embedding(d) *
                        number of occurences of word in f *  (<--- changed! occurences of word in TRAIN)
                        log inverse document frequency of word  ( number of docs / number of docs in which word appears)
                    }

        So, for each file are created 'number of domains' values of importance.
        """
        c = Chrono("Computing domain importance...")

        file2wc = self.dm.id_file2word2number_words

        for dataset in [TRAIN, DEV]:
            cache_file = dataset + DOMAIN_IMPORTANCE_CACHE_FILE

            cache = Utils.load_pickle(cache_file, folder=CACHE_FOLDER_CLASSIFIER)
            if cache:
                c.millis("loaded cache")
                self.id_file2domain2domain_importance[dataset] = cache
                continue

            self.id_file2domain2domain_importance[dataset] = {}

            for file in tqdm.tqdm(self.dm.files_ids[dataset], total=len(self.dm.files_ids[dataset])):
                self.id_file2domain2domain_importance[dataset][file] = Counter()

                for domain in self.dm.list_domains:
                    for word in file2wc[dataset][file]:
                        local_imp = self._calculate_domain_word_importance(domain, word)
                        if local_imp:
                            self.id_file2domain2domain_importance[dataset][file][domain] += local_imp

            Utils.save_pickle(self.id_file2domain2domain_importance[dataset], cache_file, folder=CACHE_FOLDER_CLASSIFIER)
        c.millis()

    def compute_string2vec(self, string):
        """
        computes the vector representation of the string
        :param string: the string
        :return: the vector representation
        """
        embed = self.compute_sentence_embedding(string)
        if  embed is None:
            return None
        domain_importance = self.compute_sentence_domain_importance(string)
        imp = [domain_importance[domain] for domain in self.run_info.list_domains]
        cos = [self.cos_sim(embed, self.domain2embedding[domain]) for domain in self.run_info.list_domains]
        return numpy.concatenate((embed, imp, cos))

    def compute_sentence_embedding(self, string):
        """
        Computes the average of the words in the sentence
        :param string:
        :return:
        """
        a = [self.lookup(word) for word in string]
        return numpy.average(a, axis=0) if a else None

    def compute_sentence_domain_importance(self, string):
        """
        computes the importance of each domain to the sentence
        :param string: the sentence
        :return: the vector of importances
        """
        string_domain_importance = Counter()
        for domain in self.run_info.name_domain2token_domain:
            for word in string:
                local_imp = self._calculate_domain_word_importance(domain, word)
                if local_imp:
                    string_domain_importance[domain] += local_imp
        return string_domain_importance

    def _calculate_domain_word_importance(self, domain, word):
        """
        Computes the importance of a given domain with respect to a single word

        The formula has been made "invariant" to the length of the sentence/file.
        The tf (and idf) are computed over the whole TRAIN set.

            imp(domain, word) =
                10^cos_sim(domain, word)
              * 1 + log( #occurences of word in TRAIN)
              * 1 + log( (#of files in TRAIN) / (#of files containing word))

        :param domain: a domain
        :param word: a word
        :return: the importance of that domain for the word
        """

        if (domain, word) in self.caching_importance:
            current_similarity = self.caching_importance[(domain, word)]
        else:
            current_similarity = Metrics.cos_sim(self.domain2embedding[domain], self.lookup(word))
            self.caching_importance[(domain, word)] = current_similarity

        if current_similarity >= self.reference_cos_sim:
            local = math.exp(10 * current_similarity)
            tf = 1 + (math.log(self.run_info.domain2word2total_count[TRAIN][domain][word]) if
                      self.run_info.domain2word2total_count[TRAIN][domain][word] else 0)
            idf = 1 + (math.log(self.run_info.number_of_files[TRAIN] / self.run_info.word2doc_frequency[TRAIN][word]) if
                       self.run_info.word2doc_frequency[TRAIN][word] else 0)
            return local * tf * idf
        return None


class Classifier:
    """
    The class that provides the methods to classify file or sentences.
    """

    @staticmethod
    def get_train_and_test_data():
        """
        Computer the x_train and y_train for the TRAIN, DEV and TEST.
        Uses a pickle cache to speedup consecutive runs.
        """
        data = Utils.load_pickle(TRAINING_DATA_CACHE, folder=CACHE_FOLDER_CLASSIFIER)
        if data:
            return data

        metrics = Metrics(load_only_run_information=False, compute_files_vectors=True)

        data = {
            X_TRAIN: metrics.id_file2vec[TRAIN],
            Y_TRAIN: [metrics.dm.id_file2domain[TRAIN][file] for file in metrics.dm.files_ids[TRAIN]],

            X_DEV: metrics.id_file2vec[DEV],
            Y_DEV: [metrics.dm.id_file2domain[DEV][file] for file in metrics.dm.files_ids[DEV]],


            TRAIN_FILES_IDS: metrics.dm.files_ids[TRAIN],
            DEV_FILES_IDS: metrics.dm.files_ids[DEV],

            TRAIN_FILENAMES: metrics.dm.files_names[TRAIN],
            DEV_FILENAMES: metrics.dm.files_names[DEV],

            TRAIN_IDFILE_DOMAIN: metrics.dm.id_file2domain[TRAIN],
            DEV_IDFILE_DOMAIN: metrics.dm.id_file2domain[DEV],

        }

        Utils.save_pickle(data, TRAINING_DATA_CACHE, folder=CACHE_FOLDER_CLASSIFIER)
        return data

    @staticmethod
    def classify_based_only_on_domain_sim():
        print("CLASSIFICATION BASED ON SIMILARITY")
        metrics = Metrics()

        unk_counter = 0

        predicted = []
        true = [metrics.dm.id_file2domain[DEV][file] for file in metrics.dm.files_ids[DEV]]

        for file in tqdm.tqdm(metrics.dm.files_ids[DEV]):
            if metrics.id_file2domain2domain_importance[DEV][file]:
                cos = [metrics.cos_sim(metrics.id_file2embedding[DEV][file], metrics.domain2embedding[domain]) for domain in
                       metrics.dm.list_domains]

                file_classification = max(list(zip(metrics.dm.list_domains, cos),), key=lambda x: x[1])[0]
                predicted.append(file_classification)
            else:
                predicted.append(random.sample(metrics.dm.list_domains, 1)[0])
                unk_counter += 1

        print(sklearn.metrics.classification_report(true, predicted))

    @staticmethod
    def classify_based_only_on_domain_importance():
        """
        Performs a classification on the DEV where the only information used
        that comes from TRAIN is the list of domain name.

        It uses the most important domain as the predicted class
        """
        print("CLASSIFICATION BASED ON IMPORTANCE")
        metrics = Metrics()

        unk_counter = 0

        predicted = []
        true = [metrics.dm.id_file2domain[DEV][file] for file in metrics.dm.files_ids[DEV]]

        for file in tqdm.tqdm(metrics.dm.files_ids[DEV]):
            if metrics.id_file2domain2domain_importance[DEV][file]:
                file_classification = max(metrics.id_file2domain2domain_importance[DEV][file], key=metrics.id_file2domain2domain_importance[DEV][file].get)
                predicted.append(file_classification)
            else:
                predicted.append(random.sample(metrics.dm.list_domains, 1)[0])
                unk_counter += 1

        print(sklearn.metrics.classification_report(true, predicted))

    @staticmethod
    def train_xgboost_classifier():
        time_id = time.strftime("%d-%m-%Y_%Hh-%Mm")
        run_id = "result_training_summary_{}".format(time_id)
        c = Chrono("Training {}...".format(run_id))

        result_summary = {"id": run_id}

        # Get the train data

        data = Classifier.get_train_and_test_data()

        x_train = numpy.array(data[X_TRAIN])
        y_train = numpy.array(data[Y_TRAIN])

        x_dev = numpy.array(data[X_DEV])
        y_dev = numpy.array(data[Y_DEV])

        # -----------------------------------------------------------------------------------------------
        # Train a XGBoost classifier.
        # The tuning of the parameters has been made by hand, looking at the loss during the training.
        # A grid search woulg have required too much time.

        classifier = xgboost.XGBClassifier(n_estimators=5000,
                                           learning_rate=0.01,
                                           objective="multi:softmax",
                                           min_child_weight=2,
                                           max_depth=8,
                                           colsample_bytree=0.4,
                                           subsample=0.8,
                                           silent=True,
                                           nthread=12
                                           )
        result_summary["classifier_params"] = classifier.get_params()
        print(json.dumps(classifier.get_params(), indent=4))

        classifier.fit(x_train, y_train, early_stopping_rounds=500, eval_metric=["merror"], eval_set=[(x_dev, y_dev)],
                       verbose=True)
        c.millis()
        Utils.save_pickle(classifier, CLASSIFIER_TRAINED_MODEL, folder=".")

        # -----------------------------------------------------------------------------------------------

        prediction_dev = classifier.predict(x_dev, ntree_limit=classifier.best_ntree_limit)

        result_summary["y_true"] = y_dev
        result_summary["y_pred"] = prediction_dev

        result_summary["dev_filenames"] = data[DEV_FILENAMES]

        precision, recall, fmeasure, support = sklearn.metrics.precision_recall_fscore_support(y_dev, prediction_dev)
        result_summary["classes"] = classifier.classes_
        result_summary["precision"] = precision
        result_summary["recall"] = recall
        result_summary["f-measure"] = fmeasure
        result_summary["support"] = support

        precision_avg, recall_avg, fmeasure_avg, _ = sklearn.metrics.precision_recall_fscore_support(y_dev,
                                                                                                     prediction_dev,
                                                                                                     average="weighted")
        result_summary["precision_average"] = precision_avg
        result_summary["recall_average"] = recall_avg
        result_summary["f-measure_average"] = fmeasure_avg

        classification_report = sklearn.metrics.classification_report(y_dev, prediction_dev)
        result_summary["classification_report"] = classification_report
        print(classification_report)

        confusion_matrix = sklearn.metrics.confusion_matrix(y_dev, prediction_dev)
        result_summary["confusion_matrix"] = confusion_matrix

        Utils.save_pickle(result_summary, run_id + ".pickle", folder=".")
        return classifier

    def interactive_testing(self):
        """
        Interactive predictions of sentence
        """
        import matplotlib.pyplot as pyplot
        pyplot.style.use("ggplot")

        # xgboost.plot_importance(classifier)
        # print(classifier.get_params())
        # print(classifier.feature_importances_)

        pyplot.bar(range(len(self.classifier.feature_importances_)), self.classifier.feature_importances_)
        pyplot.show()
        while True:
            s = input("Enter the phrase: ")
            pred, proba = self.predict_both(s, verbose=True)
            if not pred:
                continue
            print("PREDICTED: ", pred)
            self.print_top_n_predictions(self.classifier.classes_, proba, 5)
            print()

    instance = None

    @staticmethod
    def get_instance():
        """
        Singleton pattern
        :return: an instance of the class
        """
        if not Classifier.instance:
            Classifier.instance = Classifier()
        return Classifier.instance

    def __init__(self, classifier=CLASSIFIER_TRAINED_MODEL):
        self.metrics = Metrics(load_only_run_information=True, compute_files_vectors=False)
        c = Chrono("Loading classifier...")
        self.classifier = Utils.load_pickle(classifier)
        c.millis()

    def _tokenize_string(self, string):
        """
        tokenizes a string according to the tokenizer
        :param string: string to tokenize
        :return: tokenized string
        """
        a = tokenizer.tokenize_line_google(string, word2vec.get())
        return a

    def predict(self, string=None, features_vector=None):
        """
        Predicts the domain of a string
        :param string: string to predict
        :param features_vector: pre-computed features vector
        :return: predicted domain
        """
        assert string or features_vector is not None
        if features_vector is None:
            features_vector = self.metrics.compute_string2vec(self._tokenize_string(string))
        if features_vector is None:
            return None

        return self.classifier.predict([features_vector], ntree_limit=self.classifier.best_ntree_limit)

    def predict_proba(self, string=None, features_vector=None):
        """
        Predicts the domain distribution of a string
        :param string: the string
        :param features_vector: pre-computed features vector
        :return: predicted domain distribution
        """
        assert string or features_vector is not None
        if features_vector is None:
            features_vector = self.metrics.compute_string2vec(self._tokenize_string(string))
        if features_vector is None:
            return None

        return self.classifier.predict_proba([features_vector], ntree_limit=self.classifier.best_ntree_limit)[0]

    def predict_both(self, string, verbose=False):
        """
        Predicts the domain and the domain distribution of a string
        :param string: the string
        :param verbose: whether to be verbose about the tokenization
        :return: the predicted domain and domain distribution
        """
        tokenized = self._tokenize_string(string)
        if not tokenized:
            print("WARNING: ", string, tokenized)
            return None, None
        if verbose:
            print(tokenized)
        features_vector = self.metrics.compute_string2vec(tokenized)
        return self.predict(None, features_vector), self.predict_proba(None, features_vector)

    @staticmethod
    def top_n_prediction(classes, proba, n):
        """
        Get the top n predictions
        :param classes: the classes
        :param proba: the probability distribution
        :param n: the number of predictions
        :return: the n most probable classes
        """
        return sorted(list(zip(classes, proba)), key=lambda x: x[1], reverse=True)[:n]

    @staticmethod
    def print_top_n_predictions(classes, proba, n):
        """
        print the top n predictions
        :param classes: the classes
        :param proba: the probability distribution
        :param n: the number of predictions
        :return: the n most probable classes
        """
        print("Ranking:")
        topn = Classifier.top_n_prediction(classes, proba, n)
        for x, y in topn:
            print("\t", x, y)
        return topn


if __name__ == "__main__":
    Classifier.get_instance().interactive_testing()
    # Classifier_utils.train_xgboost_classifier()