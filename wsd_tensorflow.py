from collections import Counter
import numpy as np
import time
import tqdm

import tensorflow as tf

from wsd_loader import Loader, ID, LEMMA, WORD, POS
from babelnet_importer import BabelMap
import domain_classifier as domain_classifier
from wsd_tensorflow_provider import Provider
from wsd_mfs import MFS
from wsd_constants import *


DEFAULT_TENSORBOARD_FOLDER = "./wsd_tensorboard/"
TENSORFLOW_MODEL = "./wsd_tensorflow_model/model.ckpt"

SAMPLE_SIZE = 64
PREDICTIONS_REPETITIONS = 101  # 1001

EMBEDDINGS_DIM = 300
INPUT = 604

HIDDEN_LAYER_1 = 512
HIDDEN_LAYER_2 = 512

TIME_ID = time.strftime("%d-%m-%Y_%Hh-%Mm")


class WSD:
    """
    Uses the neural network trained in the homework2 to perform arbitrary wsd
    """

    def __init__(self):
        self.provider = Provider.get_instance()
        self.mfs = MFS()

        self.words_in_domain = self.provider.words_in_domaincontext_numeric_padded
        self.words_in_domain_mask = self.provider.words_in_domaincontext_numeric_mask
        self.number_domains = len(self.words_in_domain)

        self.domains_name = self.provider.domains_name
        self.domains_index = list(range(len(self.provider.domains_name)))

        self.nn_classes_size = len(self.provider.classes_list)

        self.sess = None
        self.define_network()

    def define_network(self):
        """
        Defines the tensorflow model
        """
        with tf.name_scope('embeddings'):
            #  the variable containing the google embeddings
            self.embeddings = tf.Variable(np.asarray(self.provider.embeddings_list_used), trainable=False,
                                          name="google",
                                          dtype=tf.float32)

        with tf.name_scope('domains'):
            #  variables containing the words domain, their mask and the size of each domain.
            self.domain_words = tf.Variable(np.asarray(self.words_in_domain), trainable=False, name="words",
                                            dtype=tf.int32)
            self.domain_words_mask = tf.Variable(np.asarray(self.words_in_domain_mask), trainable=False, name="mask",
                                                 dtype=tf.int32)
            self.domains_size = tf.reduce_sum(tf.cast(self.domain_words_mask, tf.int32), axis=1, name="sizes")

        with tf.name_scope('inputs'):
            #  the placeholders to feed data into the network
            self.x_train_data_domain_distribution = tf.placeholder(dtype=tf.float32, shape=[None, self.number_domains],
                                                                   name="train_data_domain_distribution")
            self.x_train_data_pos = tf.placeholder(dtype=tf.int32, shape=[None], name="train_data_pos")
            self.x_train_data_words = tf.placeholder(dtype=tf.int32, shape=[None], name="train_data_words")
            self.y_train_classes = tf.placeholder(dtype=tf.int32, shape=[None], name="y_train")

        with tf.name_scope('sampling_domain_words'):
            #  operations to perform the sampling of the domain words according to the sentence domain distribution

            #  log probabilities must be used
            self.log_prob = tf.log(self.x_train_data_domain_distribution)

            #  get the number of samples that must be done in each domain
            self.domain_distribution = tf.multinomial(self.log_prob, SAMPLE_SIZE, output_dtype=tf.int32)

            def _uniform_sampling(single_multinomial):
                """
                for each number in the domain_distributino, perform a random sampling in the correspondent domain.
                :param single_multinomial: a list of integer representing domains. For each integer a word must be sampled from that domain
                :return: the sampled words
                """
                return tf.map_fn(
                    lambda x: self.domain_words[x, tf.random_uniform((), maxval=self.domains_size[x], dtype=tf.int32)],
                    single_multinomial, tf.int32)

            # for each sample in the batch, apply the function uniform_sampling
            self.word_samples = tf.map_fn(lambda x: _uniform_sampling(x), self.domain_distribution, dtype=tf.int32)

        with tf.name_scope('average_domain_direction'):
            #  take the average of the sampled words
            self.word_embeddings = tf.nn.embedding_lookup(self.embeddings, self.word_samples)
            self.words_average = tf.reduce_mean(self.word_embeddings, [1])

        with tf.name_scope("preprocessing"):
            #  the input is made of:

            # embedding of the word to disambiguate
            self.embds = tf.nn.embedding_lookup(self.embeddings, self.x_train_data_words)

            # POS of the word to disambiguate
            self.pos_one_hot = tf.one_hot(self.x_train_data_pos, len(self.provider.poses), axis=1, dtype=tf.float32)

            # average of the sample words, concatenated togheter.
            self.input_concatenation = tf.concat([self.embds, self.pos_one_hot, self.words_average], axis=1)

        with tf.name_scope("train_netowork"):
            weights = {
                'h1': tf.Variable(tf.random_normal([INPUT, HIDDEN_LAYER_1], dtype=tf.float32), dtype=tf.float32),
                'h2': tf.Variable(tf.random_normal([HIDDEN_LAYER_1, HIDDEN_LAYER_2], dtype=tf.float32),
                                  dtype=tf.float32),
                'out': tf.Variable(tf.random_normal([HIDDEN_LAYER_2, self.nn_classes_size], dtype=tf.float32),
                                   dtype=tf.float32)
            }

            biases = {
                'b1': tf.Variable(tf.random_normal([HIDDEN_LAYER_1], dtype=tf.float32), dtype=tf.float32),
                'b2': tf.Variable(tf.random_normal([HIDDEN_LAYER_2], dtype=tf.float32), dtype=tf.float32),
                'out': tf.Variable(tf.random_normal([self.nn_classes_size], dtype=tf.float32), dtype=tf.float32)
            }

            # the first layer with the activation
            self.layer_1 = tf.add(tf.matmul(self.input_concatenation, weights['h1']), biases['b1'])
            self.layer_1 = tf.nn.tanh(self.layer_1)

            # the second layer with the activation
            self.layer_2 = tf.add(tf.matmul(self.layer_1, weights['h2']), biases['b2'])
            self.layer_2 = tf.nn.tanh(self.layer_2)

            # the output layer
            self.pred = tf.matmul(self.layer_2, weights['out']) + biases['out']

        #  manage the save and restore
        self.saver = tf.train.Saver()

        #  finalize the graph to avoid adding other nodes
        tf.get_default_graph().finalize()

    def print_debug(self, x_train, domain_distribution, words):
        """
        Prints some information about the sampled words
        :param x_train:  the probability distribution of the sentence (ground truth)
        :param domain_distribution: the domain distribution resulting from the multinomial in tf
        :param words: the sampled words in tf
        """
        for proba, (i_res, res), b in zip(x_train, enumerate(domain_distribution), words):
            topn = domain_classifier.Classifier.print_top_n_predictions(self.domains_name, proba, 5)
            topn = [x for x, _ in topn]
            print(res)
            for i_domain, domain in enumerate(self.domains_name):
                if domain in topn:
                    c = Counter(res)
                    print(
                        "{}\n\tOriginal distribution: {}\n\tDistribution resulting from the multinomial: {} ({}/{})\n".format(
                            domain,
                            x_train[i_res][i_domain],
                            c[i_domain] / SAMPLE_SIZE, c[i_domain], len(res)))
            print(np.asarray(list(self.provider.reverse_dictionary[x] for x in b)))
            print()

    def open_session(self):
        """
        Opens a tensorflow session
        """
        if self.sess is not None:
            raise RuntimeError("One session at time.")
        self.sess = tf.Session()
        self.saver.restore(self.sess, TENSORFLOW_MODEL)

    def close_session(self):
        """
        Closes a tensorflow session
        """
        if self.sess is None:
            raise RuntimeError("No session to close")
        self.sess.close()
        self.sess = None

    def disambiguate_sentence(self, sentence_restored, word_original_list, lemma_original_list, pos_original_list):
        """
        Perform the disambiguation of a sentence.
        :param sentence_restored: the sentence to disambiguate, de-tokenized
        :param word_original_list: the words of the sentence to disambiguate as is
        :param lemma_original_list: the lemmas of the sentence to disambiguate as is
        :param pos_original_list: the pos of the sentence to disambiguate as is
        :return: a list of synsets (None if not available), and the distribution of probability of the sentence over the domains.
        """
        pred, proba = self.provider.classifier.predict_both(sentence_restored) # todo can be None if  if all the words in a sentence aren't in the google embeddings.
        if proba is None:
            print("Warning: proba is None, so all the words in the sentence are not in the google embeddings!")
            print("\t", sentence_restored)
            print("\t", word_original_list)
            print("\t", lemma_original_list)
            print("\t", pos_original_list)
            return [None]*len(word_original_list), None
        # assert proba is not None

        word_list = []
        lemma_list = []
        pos_list = []
        distr_list = []
        id_list = []

        for index, (word, lemma, pos) in enumerate(zip(word_original_list, lemma_original_list, pos_original_list)):
            if lemma in self.provider.dictionary and pos in self.provider.poses_dictionary:
                l = self.provider.dictionary[lemma]
                w = self.provider.dictionary[word] if word in self.provider.dictionary else l
                word_list.append(w)
                lemma_list.append(l)
                pos_list.append(self.provider.poses_dictionary[pos])
                distr_list.append(proba)
                id_list.append(index)

        if not word_list:
            return [None]*len(word_original_list), None

        predictions = self.most_commont_prediction(self.default_pred, word_list, lemma_list, pos_list, distr_list, id_list)
        # print(predictions)
        # for a, b, c in zip(word_list, pos_list, [predictions[x] for x in sorted(predictions)]):
        #     print("{}\t{}\t{}".format(self.provider.reverse_dictionary[a], self.provider.poses[b], c))

        pred_list = [None]*len(word_original_list)
        for index in predictions:
            pred_list[index] = predictions[index]
        return pred_list, proba

    def most_commont_prediction(self, predictor, word_list, lemma_list, pos_list, distr_list, id_list):
        """
        Performs PREDICTIONS_REPETITIONS predictioncs using predictor, then for each ID takes the most common.
        :param predictor: the predictor to use: self.predict_dev or self.predict_test
        :param sess: the session
        :param tfpred: the output layer of the network to use
        :param tword: the placeholder for the words
        :param tpos: the placeholder for the pos
        :param tdom: the placeholder for the domain
        :param tsen: the placeholder for the sentences id
        :return: the predictions
        """
        # print("Predicting", predictor.__name__, "with the most common strategy")
        tot_pred = {}

        for x in range(PREDICTIONS_REPETITIONS):
            predictions = predictor(word_list, lemma_list, pos_list, distr_list, id_list)
            tot_pred[x] = predictions

        finalpred = {}
        for x in tot_pred[0]:
            counter = Counter(tot_pred[i][x] for i in tot_pred)
            finalpred[x] = counter.most_common(1)[0][0]
        return finalpred

    def default_pred(self, word_list, lemma_list, pos_list, distr_list, id_list):
        """
        The default method to perform the predictions. Consists of running the model, decode the predictions.
        :param word_list: the list of words
        :param lemma_list: the list of lemmas
        :param pos_list: the list of pos
        :param distr_list: the list of probability distribution over the domains
        :param id_list: the list of ids (uniquely identify an instance)
        :return: the performed predictions
        """
        pred = self.sess.run(self.pred, feed_dict={self.x_train_data_words: word_list,
                                                   self.x_train_data_pos: pos_list,
                                                   self.x_train_data_domain_distribution: distr_list})

        predictions = self.perform_predictions(pred, word_list, id_list, pos_list, lemma_list)
        return predictions

    def perform_predictions(self, pred, list_words, list_id, list_pos, list_lemma):
        """
        Given a the result of the network pred, predicts the class for each word.
        Moreover if some predictions is missing, it is used the default predictor (see the mfs class) to get it.
        :param pred: result of the nn
        :param list_words: list of words (numerical)
        :param list_id: list of the id (string) of the words
        :param list_pos: list of the pos (numerical) of the words
        :param list_lemma: the list of the lemmas
        :return: the predictions
        """
        predictor = self.mfs.predictor()

        predictions = {}
        for id_word, idd, pos, proba, id_lemma in zip(list_words, list_id, list_pos, pred, list_lemma):
            word = self.provider.reverse_dictionary[id_word]
            lemma = self.provider.reverse_dictionary[id_lemma]
            pos = self.provider.poses[pos]
            entry = (word, pos)

            # assert entry in self.provider.loader.word_pos2synsets[TRAIN]
            if (entry in self.provider.loader.word_pos2synsets[TRAIN]
                and len(self.provider.loader.word_pos2synsets[TRAIN][entry]) > 1):
                possible_classes = [self.provider.classes_dictionary[i] for i in
                                    self.provider.loader.word_pos2synsets[TRAIN][entry]
                                    if i in self.provider.classes_dictionary]

                if not possible_classes:  # todo: should not be possible.
                    predictions[idd] = predictor(word, lemma, pos)
                    print("Warning: {} - {} not in class dictionary. Predicted: {}".
                          format(entry, self.provider.loader.word_pos2synsets[TRAIN][entry], predictions[idd]))
                    # print("")
                    continue
                max_class = max(possible_classes, key=lambda x: proba[x])
                predictions[idd] = self.provider.reverse_classes_dictionary[max_class]

            else:
                predictions[idd] = predictor(word, lemma, pos)

        return predictions


if __name__ == '__main__':
    w = WSD()
    w.open_session()
    w.disambiguate_sentence("I play I tennis", ["I", "play", "I", "tennis", ], ["I", "play", "I", "tennis", ], ["PRON", "VERB", "PRON", "NOUN"])
    w.close_session()
