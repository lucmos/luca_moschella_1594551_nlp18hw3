import srl_constants
import utils
from chronometer import Chrono

from wsd_tensorflow_provider import Provider
from babelnet_importer import BabelMap


class Disambiguator:
    """
    Manages the disambiguation of the ConLL 2009 dataset.
    """

    instance = None

    @staticmethod
    def get_instance(renew=False):
        """
        Singleton pattern, with cache and possibility to renew it
        :param renew: Whether to renew the cache
        :return: an instance of this class
        """

        if Disambiguator.instance and not renew:
            return Disambiguator.instance

        c = Chrono("Loading conll disambiguated...")
        cache = None
        if renew:
            Disambiguator.instance = Disambiguator()
        else:
            cache = utils.load_pickle(srl_constants.CACHE_CONLL_DISAMBIGUATED)
            Disambiguator.instance = cache if cache else Disambiguator()

        if not cache:
            c.millis("generated")
            utils.save_pickle(Disambiguator.instance, srl_constants.CACHE_CONLL_DISAMBIGUATED, override=renew)
        else:
            c.millis("cached")
        return Disambiguator.instance

    def __init__(self):
        self.disambiguated_conll = {x: [] for x in srl_constants.DATASETS}
        self.sentences_proba_conll = {x: [] for x in srl_constants.DATASETS}

        self.pos_normalization = {
            "#": "SYM",
            "$": "SYM",
            "''": ".",
            ",": ".",
            "-LRB-": ".",
            "-RRB-": ".",
            ".": ".",
            ":": ".",
            "AFX": "ADJ",
            "CC": "CCONJ",
            "CD": "NUM",
            "DT": "DET",
            "EX": "PRON",
            "FW": "X",
            "HYPH": ".",
            "IN": "ADP",
            "JJ": "ADJ",
            "JJR": "ADJ",
            "JJS": "ADJ",
            "LS": "X",
            "MD": "VERB",
            "NIL": "X",
            "NN": "NOUN",
            "NNP": "NOUN",
            "NNPS": "NOUN",
            "NNS": "NOUN",
            "PDT": "DET",
            "POS": "PRT",
            "PRP": "PRON",
            "PRP$": "DET",
            "RB": "ADV",
            "RBR": "ADV",
            "RBS": "ADV",
            "RP": "ADP",
            "SYM": "SYM",
            "TO": "PRT",
            "UH": "INTJ",
            "VB": "VERB",
            "VBD": "VERB",
            "VBG": "VERB",
            "VBN": "VERB",
            "VBP": "VERB",
            "VBZ": "VERB",
            "WDT": "DET",
            "WP": "PRON",
            "WP$": "DET",
            "WRB": "ADV",
            "``": ".",
            "(": ".",
            ")": "."
        }
        self.perfom_disambiguation()

    def _convert_pos(self, pos):
        """
        Convert the POS representation
        :param pos: a pos as in ConLL 2009
        :return: a pos as in HW2
        """
        if pos not in self.pos_normalization:
            # print("WARNING: {} not recognized".format(pos))
            return pos
        return self.pos_normalization[pos]

    def _normalize_pos(self, list_poses):
        """
        Converts a list of POSes
        :param list_poses: a list of POS
        :return: the converted list
        """
        return [self._convert_pos(x) for x in list_poses]

    def perfom_disambiguation(self):
        """
        Performs the disambiguation of the dataset
        """
        import wsd_tokenizer
        from wsd_tensorflow import WSD
        from srl_data_loader import Loader
        from tqdm import tqdm

        disambiguator = WSD()
        srl_loader = Loader.get_instance()

        disambiguator.open_session()

        for dataset in srl_constants.DATASETS:
            print(dataset)
            for i_sen, sentence in enumerate(tqdm(srl_loader.data[dataset][srl_constants.FORM])):
                words = srl_loader.data[dataset][srl_constants.FORM][i_sen]
                lemmas = srl_loader.data[dataset][srl_constants.LEMMA][i_sen]
                poses = srl_loader.data[dataset][srl_constants.POS][i_sen]
                poses = self._normalize_pos(poses)

                restored_sentence = wsd_tokenizer.detokenize(words, poses)

                disa, proba = disambiguator.disambiguate_sentence(restored_sentence, words, lemmas, poses)

                assert disa is not None
                assert len(disa) == len(words)

                self.disambiguated_conll[dataset].append(disa)
                self.sentences_proba_conll[dataset].append(proba)

                #
                # print(restored_sentence)
                # print()
                # for a, b, c in zip(words, poses, disa):
                #     print("{}\t{}\t{}".format(a,b,c))
                # print()
                # Classifier.print_top_n_predictions(self.disambiguator.domains_name, proba, 10)
                # print()
                # print()

        disambiguator.close_session()


if __name__ == '__main__':
    from srl_data_loader import Loader
    d = Disambiguator.get_instance(True)
    l = Loader.get_instance()

    for dataset in srl_constants.DATASETS:
        for sentence, pred in  zip(l.data[dataset][srl_constants.FORM], d.disambiguated_conll[dataset]):
            assert len(sentence) == len(pred)
            for a,b in zip(sentence, pred):
                print("{}\t{}".format(a, b))
            print()
            print("---------------------------------------")
            print()


