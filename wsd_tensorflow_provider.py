
import utils as Utils
from babelnet_importer import BabelMap
from chronometer import Chrono
from domain_classifier import Classifier, word2vec
from wsd_loader import Loader, ID, POS, WORD, LEMMA
import wsd_tokenizer as tokenizer
from wsd_constants import *


class Provider:
    """
    This class provides all the information needed to train the neural network.
    """

    instance = None

    @staticmethod
    def get_instance(renew=False):
        """
        Singleton pattern, with cache and possibility to renew it
        :param renew: Whether to renew the cache
        :return: an instance of this class
        """

        if Provider.instance and not renew:
            return Provider.instance

        c = Chrono("Loading provider...")
        cache = None
        if renew:
            Provider.instance = Provider()
        else:
            cache = Utils.load_pickle(CACHE_FILE_PROVIDER)
            Provider.instance = cache if cache else Provider()

        if not cache:
            Utils.save_pickle(Provider.instance, CACHE_FILE_PROVIDER, override=True)
            c.millis("generated")
        else:
            c.millis("cached")
        return Provider.instance

    def __init__(self, mantain_all_datastructures=False):
        """
        :param mantain_all_datastructures: The cache does not maintain all the datastructures for efficiency.
                                            True if they must maintained.
        """

        self.loader = Loader.get_instance()
        self.babelmap = BabelMap.get_instance()
        self.classifier = Classifier.get_instance()
        self.embeddings = word2vec.get()

        self.dataset_names = [TRAIN, DEV, TEST]

        self.poses = [NOUN, VERB, ADJ, ADV]
        self.poses_dictionary = {x: ix for ix, x in enumerate(self.poses)}

        self.domains_name = self.classifier.classifier.classes_

        #  only classes associated with ambiguos words, ONLY FOR TRAIN
        self._classes_counter = 0
        self.classes_list = []
        self.classes_dictionary = {}
        self.reverse_classes_dictionary = {}

        self.ambiguos_word2classes = {}

        self._counter_words = 0
        self.word2google_word = {}
        self.dictionary = {}
        self.reverse_dictionary = {}
        self.dictionary_list = []
        self.embeddings_list_used = []

        # self._counter_nn = 0
        # self.nn_dictionary = {}
        # self.reverse_nn_dictionary = {}
        # self.nn_id2occurences = {}
        # self.nn_dictionary_list = []
        #
        # self.nn_entry_classes_mapping = {}
        # self.reverse_nn_entry_classes_mapping = {}
        # self.nn_classes_list = []

        self.to_disambiguate_words = []
        self.to_disambiguate_words_multi_sense_words_with_embeddings = []

        self.max_domain_words_len = None
        self.words_in_domaincontext = self.fit_context_words()
        self.words_in_domaincontext_numeric = []
        self.words_in_domaincontext_numeric_padded = []
        self.words_in_domaincontext_numeric_mask = []

        self.max_sentence_len = None
        self.doc_sentence_tokenized_numeric = []

        self.doc_sentence_tokenized_numeric_flatted_padded = []
        self.doc_sentence_tokenized_numeric_flatted_mask = []
        self.sentence2docsentence = {}
        self.docsentence2sentence = {}

        self._create_word_dictionaries()
        self._memorize_ambiguos_word_positions()
        self._convert_into_numeric()
        self._generate_sentences_padded()
        self._generate_domain_words_padded()

        if not mantain_all_datastructures:
            del self.embeddings
            del self.babelmap

            del self.words_in_domaincontext
            del self.words_in_domaincontext_numeric

            del self.doc_sentence_tokenized_numeric

    @staticmethod
    def _pad_list(sentence, max_len):
        """
        pads a list to be long max_len
        :param sentence: the list to be padded
        :param max_len: the final length of the list
        :return: the padded list
        """
        sentence_len = len(sentence)
        assert sentence_len <= max_len
        return sentence + ([0] * (max_len - sentence_len))

    @staticmethod
    def _mask_list(sentence, max_len):
        """
        Generates a mask that is True on the sentence, and False on the padding.
        :param sentence: the original sentence
        :param max_len: the final length of the sentence
        :return: the mask of the sentence
        """
        sentence_len = len(sentence)
        assert sentence_len <= max_len
        # print(sentence)
        # print([True]*sentence_len + [False]*(max_len - sentence_len))
        # assert False
        return [True]*sentence_len + [False]*(max_len - sentence_len)

    def _generate_domain_words_padded(self):
        """
        Pads the domain words, to have all the same length. Uses a mask to recognize real words.
        """
        c = Chrono("Generating padded domain words...")
        self.max_domain_words_len = len(max(self.words_in_domaincontext_numeric, key=len))
        self.words_in_domaincontext_numeric_padded = [self._pad_list(domain, self.max_domain_words_len) for domain in self.words_in_domaincontext_numeric]
        self.words_in_domaincontext_numeric_mask = [self._mask_list(domain, self.max_domain_words_len) for domain in self.words_in_domaincontext_numeric]
        c.millis("done")

    def _generate_sentences_padded(self):
        """
        Pads the sentences, to have all the same length. Uses a mask to recognize real words.
        """
        c = Chrono("Generating padded sentences...")
        self.max_sentence_len = 0
        for dataset in self.doc_sentence_tokenized_numeric:
            for doc in dataset:
                curr_max = len(max(doc, key=len))
                self.max_sentence_len = curr_max if curr_max > self.max_sentence_len else self.max_sentence_len

        sentence_id = 0
        for i_dataset, dataset in enumerate(self.doc_sentence_tokenized_numeric):
            for i_doc, doc in enumerate(dataset):
                for i_sen, sen in enumerate(doc):
                    self.doc_sentence_tokenized_numeric_flatted_padded.append(self._pad_list(sen, self.max_sentence_len))
                    self.doc_sentence_tokenized_numeric_flatted_mask.append(self._mask_list(sen, self.max_sentence_len))

                    a = (i_dataset, i_doc, i_sen)
                    self.sentence2docsentence[sentence_id] = a
                    self.docsentence2sentence[a] = sentence_id
                    sentence_id += 1
        c.millis("done")

    def fit_context_words(self, pointers_type=DOMAIN2WORDS):
        """
        Transforms the domain words in order to ensure they are present in the google embeddings.
        :param pointers_type: which domain words to transform
        """
        data = self.babelmap.domain2context[pointers_type]
        domain_words = []

        for domain in self.domains_name:
            domain_list = []
            for x in data[domain]:
                word_in_google = tokenizer.fit_word_into_google(x, word2vec.get())
                if word_in_google:
                    domain_list.append(word_in_google)
            domain_words.append(domain_list)
        return domain_words

    def _add_word_to_dictionary(self, initial_word):
        """
        add a word to the dictionary
        :param initial_word: the word to add
        """
        if not initial_word or initial_word in self.dictionary:
            return None

        word = tokenizer.fit_word_into_google(initial_word, self.embeddings)
        if not word or word in self.dictionary:
            return None

        self.word2google_word[initial_word] = word

        self.dictionary[word] = self._counter_words
        self.reverse_dictionary[self._counter_words] = word

        self.dictionary_list.append(word)
        self.embeddings_list_used.append(self.embeddings[word])
        self._counter_words += 1

    def _create_word_dictionaries(self):
        """
        creates the words dictionary
        """
        for i_dataset, dataset in enumerate(self.dataset_names):
            for i_doc, doc in enumerate(self.loader.doc_sentences[dataset][ID]):
                for i_sen, sentence in enumerate(self.loader.doc_sentences[dataset][ID][i_doc]):
                    for word in self.loader.doc_sentences_tokenized[dataset][i_doc][i_sen]:
                        self._add_word_to_dictionary(word)

                    for i_word, word in enumerate(self.loader.doc_sentences[dataset][WORD][i_doc][i_sen]):
                        lemma = self.loader.doc_sentences[dataset][LEMMA][i_doc][i_sen][i_word]
                        self._add_word_to_dictionary(word)
                        self._add_word_to_dictionary(lemma)

            for i_domain, domain in enumerate(self.domains_name):
                for word in self.words_in_domaincontext[i_domain]:
                    self._add_word_to_dictionary(word)

    def _add_classes(self, classes, word):
        """
        add a class list to the class dictionary
        :param classes: the list of class to be added
        :param word: the word to which they refer
        """
        self.ambiguos_word2classes[word] = list(classes)
        for c in classes:
            if c in self.classes_dictionary:
                continue

            self.classes_dictionary[c] = self._classes_counter
            self.reverse_classes_dictionary[self._classes_counter] = c
            self.classes_list.append(c)
            self._classes_counter += 1

    def _memorize_ambiguos_word_positions(self):
        """
        memorize the position of ambiguous words for fast access.
        memorize the position of ambiguous multisemous words that are present in the google embeddings, for fast access.
        """
        for dataset in self.dataset_names:
            dataset_list_to_disambiguate = []
            dataset_list_multisense_words_with_embeddings = []
            for i_doc, doc in enumerate(self.loader.doc_sentences[dataset][ID]):
                for i_sen, sentence in enumerate(self.loader.doc_sentences[dataset][ID][i_doc]):

                    for i_id, idd in enumerate(self.loader.doc_sentences[dataset][ID][i_doc][i_sen]):
                        if idd:
                            dataset_list_to_disambiguate.append((i_doc, i_sen, i_id))

                            # if dataset == TRAIN or dataset == DEV:
                            pos = self.loader.doc_sentences[dataset][POS][i_doc][i_sen][i_id]
                            word = self.loader.doc_sentences[dataset][WORD][i_doc][i_sen][i_id]
                            entry = (word, pos)
                            if entry in self.loader.word_pos2synsets[TRAIN]:  # if the ground truth is available

                                senses = self.loader.word_pos2synsets[TRAIN][entry]  # todo valuta la possiblità di usare word,pos

                                if len(senses) > 1 and word in self.word2google_word:  # if it is an ambiguos word
                                    dataset_list_multisense_words_with_embeddings.append((i_doc, i_sen, i_id))
                                    if dataset == TRAIN:
                                        self._add_classes(senses.keys(), word)

                            # self._add_nn_to_list(word, pos, i_doc, i_sen, i_id)
            self.to_disambiguate_words.append(dataset_list_to_disambiguate)
            self.to_disambiguate_words_multi_sense_words_with_embeddings.append(dataset_list_multisense_words_with_embeddings)

    def _convert_into_numeric(self):
        """
        converts the domain words and sentences to their numeric corrective.
        :return:
        """
        self.words_in_domaincontext_numeric = [[self.dictionary[word] for word in words_of_domain] for words_of_domain in self.words_in_domaincontext]

        for dataset in self.dataset_names:
            dataset_list = []
            for i_doc in range(len(self.loader.doc_sentences_tokenized[dataset])):
                doc = [[self.dictionary[word] for word in sentence] for sentence in self.loader.doc_sentences_tokenized[dataset][i_doc]]
                dataset_list.append(doc)
            self.doc_sentence_tokenized_numeric.append(dataset_list)


def test_coherence():
    """
    Checks the integrity of the data
    """
    b = Provider(mantain_all_datastructures=True)

    c = Chrono("Testing numeric coherence...")
    for i_dataset, dataset in enumerate(b.dataset_names):
        for idoc, doc in enumerate(b.loader.doc_sentences_tokenized[dataset]):
            for isen, sen in enumerate(doc):
                for iw, w in enumerate(sen):
                    num_w = b.doc_sentence_tokenized_numeric[i_dataset][idoc][isen][iw]
                    num_w2 = b.doc_sentence_tokenized_numeric_flatted_padded[b.docsentence2sentence[(i_dataset, idoc, isen)]][iw]
                    rec_w = b.reverse_dictionary[num_w]
                    rec_w2 = b.reverse_dictionary[num_w2]
                    assert w == rec_w == rec_w2, "{} = {} = {}".format(w, rec_w, rec_w2)

                for i in range(b.max_sentence_len):
                    m = b.doc_sentence_tokenized_numeric_flatted_mask[b.docsentence2sentence[(i_dataset, idoc, isen)]][i]
                    if i < len(sen):
                        assert m, "{} - {} - {} - {}\n{}".format(m, b.doc_sentence_tokenized_numeric_flatted_padded[b.docsentence2sentence[(i_dataset, idoc, isen)]][i], i, len(sen), sen)
                    else:
                        assert not m
                        assert b.doc_sentence_tokenized_numeric_flatted_padded[b.docsentence2sentence[(i_dataset, idoc, isen)]][i] == 0

    for ix, x in enumerate(b.domains_name):
        for iw, word in enumerate(b.words_in_domaincontext[ix]):
            numeric_word = b.words_in_domaincontext_numeric[ix][iw]
            numeric_word2 = b.words_in_domaincontext_numeric_padded[ix][iw]
            rec_w = b.reverse_dictionary[numeric_word]
            rec_w2 = b.reverse_dictionary[numeric_word2]
            assert word == rec_w == rec_w2, "{} = {} = {}".format(word, rec_w, rec_w2)

        for i in range(b.max_domain_words_len):
            m = b.words_in_domaincontext_numeric_mask[ix][i]

            if i < len(b.words_in_domaincontext[ix]):
                assert m
            else:
                assert not m
                assert b.words_in_domaincontext_numeric_padded[ix][i] == 0

    assert len(b.classes_list) == len(b.classes_dictionary) == len(b.reverse_classes_dictionary)
    for inum, classe in enumerate(b.classes_list):
        assert classe == b.reverse_classes_dictionary[inum]
        assert inum == b.classes_dictionary[classe]

    # for x in b.nn_dictionary_list:
    #     assert x in b.loader.word_pos2synsets[TRAIN]
    # assert len(b.nn_dictionary_list) == len(b.loader.word_pos2synsets[TRAIN])

    c.millis("OK!")


if __name__ == '__main__':
    """
    Performs the coherency check.
    """

    test_coherence()
    p = Provider.get_instance(True)
    total_words = 0
    for ix, x in enumerate(p.domains_name):
        count = len(p.words_in_domaincontext_numeric_padded[ix])
        total_words += count
        print("{}: {}\t---\t{}".format(x, count, [p.reverse_dictionary[x] for x in p.words_in_domaincontext_numeric_padded[ix][:50]]))
    print("\nNumber of total words {}".format(total_words))

