from srl_constants import *
from srl_data_loader import Loader
from srl_data_provider import Provider


class Evaluator:
    """
    Performs the evaluation of the predictions on the DEV dataset
    """

    def __init__(self):
        self.loader = Loader.get_instance()
        self.provider = Provider.get_instance()

        self.dev_ground_truth = self._get_groundtruth()
        self.dev_ground_truth_sorted = sorted(self.dev_ground_truth)

    def _get_groundtruth(self):
        """
        Reads the DEV dataset and extracts the ground truth in a comfy data structure
        :return: return the gold truth for the DEV
        """
        the_truth = {}
        for i_sen, sentence in enumerate(self.loader.data[DEV][APREDS]):
            for i_tok, token in enumerate(sentence):
                if token:  # I ignore the sentences without predicates
                    entry = (1, i_sen, i_tok)
                    apreds = [self.provider.global_dictionary[APREDS][x] for x in token]
                    the_truth[entry] = apreds
        return the_truth

    @staticmethod
    def _flatten_predictions(predictions):
        """
        Sorts the predictions in a given order and flattens the out
        :param predictions: the predictions
        :return: the predictions flattened in a 1-d array
        """
        return [apred for entry in sorted(predictions) for apred in predictions[entry]]

    def generate_confusion_matrix(self, predictions):
        """
        Generates the confusion matrix for the predictions against the ground truth
        :param predictions: the predictions to be evaluated
        :return: the confusion matrix, and the labels
        """
        ground = self.provider.decode_predictions(self._flatten_predictions(self.dev_ground_truth))
        preds = self.provider.decode_predictions(self._flatten_predictions(predictions))
        assert len(ground) == len(preds)

        import sklearn.metrics
        labels = sorted(self.provider.global_dictionary[APREDS])
        confusion_matrix = sklearn.metrics.confusion_matrix(ground, preds, labels)
        return confusion_matrix, labels

    def eval(self, predicted):  # assumes that _ is codified as 0
        """
        Computes the precision, recall and f-measure of the predictions against the ground truth
        :param predicted: the predictions to be evaluated
        :return: the measures computed
        """
        assert len(predicted) == len(self.dev_ground_truth)
        true_positive = 0

        false_positive = 0
        false_negative = 0

        for entry in self.dev_ground_truth_sorted:
            # print(entry, "\t", predicted[entry])
            for pred_label, gold_label in zip(predicted[entry], self.dev_ground_truth[entry]):

                if pred_label == gold_label:
                    if gold_label:
                        true_positive += 1
                else:
                    if pred_label:
                        false_positive += 1
                    else:
                        false_negative += 1

        precision = true_positive / (true_positive + false_positive) if true_positive + false_positive else 0
        recall = true_positive / (true_positive + false_negative) if true_positive + false_negative else 0
        fmeasure = 2 * (precision * recall) / (precision + recall) if precision + recall else 0

        print("\tprecision:\t{}\n\tRecall:\t{}\n\tf-measure\t{}\n".format(precision, recall, fmeasure))

        return precision, recall, fmeasure


if __name__ == '__main__':
    a = Evaluator()
    a.generate_confusion_matrix(a.dev_ground_truth)
