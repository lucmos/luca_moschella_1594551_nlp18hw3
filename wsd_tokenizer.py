import re
import html
import json
# load the stopwords
# with open("stopwords.json") as handle:
#     stopwords = set(json.load(handle))

MAX_PHRASE_LEN = 5
SPLITTING_STRATEGY = "^[^a-zA-Z0-9_%$€]+|[^a-zA-Z0-9_%$€]+\s|[^a-zA-Z0-9_%$€]+$|[\(\)\[\]\{\}\"\-\\\/\<\>\“\”\’\‘\—\s]"


def generate_phrase(start, i, list, model, curr_phrase="", max_len=MAX_PHRASE_LEN):
    """
    Recursive function that tries to fit a sequence of words into a Phrase available in the (google) embeddings
    :param start: start phrase search
    :param i: current index
    :param list: the sentence trasformed with phrases
    :param model: model to check the presence of a phrase
    :param curr_phrase: phrase built until now
    :param max_len: maximum lenght of the phrases to search for
    """
    assert i > 0
    if i >= len(list) or not max_len:
        return
    p = curr_phrase + "_" + list[i] if curr_phrase else list[i-1] + "_" + list[i]
    if p in model:
        del list[start:i]
        list[start] = p
        i = start
    generate_phrase(start, i + 1, list, model, p, max_len-1)


def update_sentence(line, model):
    """
    Fits the sentence "line" to have phrases instead of words
    :param line: the sentence to fit
    :param model: the model to check the presence of a phrase
    :return: the fitted line
    """
    i = 1
    while i < len(line):
        generate_phrase(i-1, i, line, model)
        i += 1
    return line


def transform_word(w):
    """
    Pre-process a word to emulate the pre-processing performed on the google embeddings
    :param w: the word to be preprocessed
    :return: the fitted word
    """
    number_of_digit = 0
    for i in w:
        if i.isdigit():
            number_of_digit += 1

    if number_of_digit <= 1:
        return w

    return re.sub("\d", "#", w)


def tokenize_line_google(line, word2vec):
    """
    Given a sentence line, tries to tokenize it according to the (unknown) tokenizer used in the google embeddings

    :param line: the line to be splitted
    :param word2vec: the google embeddings
    :return: a list of tokens (word), each one of the is present in word2vec
    """

    line_splitted_on_whitespace = [x for x in re.split("\s", line) if x]
    update_sentence(line_splitted_on_whitespace, word2vec)

    line_splitted = re.split(SPLITTING_STRATEGY, " ".join(line_splitted_on_whitespace))

    final_line = []
    for w in line_splitted:
        if not w:
            continue
        word = transform_word(w)
        response = _add(final_line, word, word2vec)
        if not response:
            b = re.split("\W", w)
            for s in b:
                s = transform_word(s)
                if s and len(s) > 1:
                    _add(final_line, s, word2vec)

    update_sentence(final_line, word2vec)
    return final_line


def _add(line, word, word2vec):
    if word and word in word2vec:
        line.append(word)
        return True
    return False


def tokenize(line, pattern, blacklist=("and",)):
    """
    Tokenizes a line according to a pattern

    :param line: the line to be splitted
    :param pattern: the pattern that delimits each word
    :param blacklist: words that must not be included
    :return: a list of tokens (word)
    """
    return [word for word in re.split(pattern, line.lower()) if word and word not in blacklist]


def fit_word_into_google(word, word2vec):
    """
    Given a word, or a word-like, tries to return the correspondent word present in the google embeddings

    :param word: word to be fitted
    :param word2vec: the google embeddings
    :return: the fitted word
    """
    if word in word2vec:
        return word
    tok = tokenize_line_google(word, word2vec)
    if len(tok) == 1 and len(tok[0]) > 1:
        return tok[0]
    else:
        tok_normalized = tokenize_line_google(word.lower(), word2vec)
        if len(tok_normalized) == 1 and len(tok_normalized[0]) > 1:
            return tok_normalized[0]
    return None


def detokenize(words, poses):
    """
    Tries to de-tokenize the sentence (e.g. did n't -> didn't), in order to use the google embeddings.
    """
    assert len(words) == len(poses)

    no_prior_space = ["."]
    no_prior_space_if_apos = ["VERB", "ADV", "PRT"]

    sentence = ""
    for word, pos in zip(words, poses):
        word = html.unescape(word.replace("&amp;amp;", "&amp;")).strip()
        space = " "
        if ((not sentence or pos in no_prior_space or (pos in no_prior_space_if_apos and "'" in word))
                and word != "``" and word != "("):
            space = ""
        sentence += space + word
    return sentence

