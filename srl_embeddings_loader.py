import gensim

from chronometer import Chrono


class Embeds:
    """
    Manages the loading of the raw embeddings, wrapping them into an object to offer a common interface.
    It uses a singleton pattern.
    """

    # todo: must choose which embeddings to use
    # EMBEDDINGS = "./embeddings/GoogleNews-vectors-negative300.bin"
    EMBEDDINGS = "./embeddings/NASARIembed+UMBC_w2v.bin"

    # todo: change accordingly.
    # Google embeddings: case senstive (=False)
    # Nasari vectore: all lowercase (=True)
    TO_LOWERCASE = True

    instance = None

    @staticmethod
    def get_instance():
        """
        :returns the embeddings.
        """
        if not Embeds.instance:
            Embeds.instance = Embeds()
        return Embeds.instance

    def __init__(self):
        c = Chrono("Loading embeddings...")
        # Load pre-trained Word2Vec model.
        self.embeddings = gensim.models.KeyedVectors.load_word2vec_format(Embeds.EMBEDDINGS, binary=True)
        c.millis()

    def __contains__(self, item):
        return Embeds._preprocess(item) in self.embeddings

    def __getitem__(self, item):
        return self.embeddings[Embeds._preprocess(item)]

    @staticmethod
    def _preprocess(item):
        """
        Determines how an item must be preprocessed before using it.
        e.g. The nasari vectors espect all lower case.
        :param item: the item
        :return: the preprocessed item
        """
        return item.lower() if Embeds.TO_LOWERCASE else item


if __name__ == '__main__':
    embeds = Embeds()

    words = ["hello", "soccer", "bn:00061450n", "Italy", "Rome"]
    for w in words:
        print(embeds[w])

    for w in words:
        print(w in embeds)

