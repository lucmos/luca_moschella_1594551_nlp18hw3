"""
Common constants used in the srl project.
"""

import os

# dictionary keys

TRAIN = "TRAIN"
DEV = "DEV"
TEST = "TEST"

DATASETS = [TRAIN, DEV, TEST]  # do not change order

# FILE PATHS

LANGUAGE_FOLDER = "SRLData/EN/"

DEV_FILE = os.path.join(LANGUAGE_FOLDER, "CoNLL2009-ST-English-development.txt")
TRAIN_FILE = os.path.join(LANGUAGE_FOLDER, "CoNLL2009-ST-English-train.txt")

TEST_FILE = os.path.join(LANGUAGE_FOLDER, "TEST/test.csv")
TEST_VERB_FILE = os.path.join(LANGUAGE_FOLDER, "TEST/testverbs.csv")

DATASETS_FILE = {TRAIN: TRAIN_FILE,
                 DEV: DEV_FILE,
                 TEST: TEST_FILE}

TEST_RESULTS = "results/test_predictions_{}.csv"




# CACHES

CACHE_LOADER = "./cache/loader.pickle"
CACHE_PROVIDER = "./cache/provider.pickle"
CACHE_CONLL_DISAMBIGUATED = "./cache/conll_disambiguated.pickle"

# COLUMNS

ID = "ID"
FORM = "FORM"
LEMMA = "LEMMA"
PLEMMA = "PLEMMA"
POS = "POS"
PPOS = "PPOS"
FEAT = "FEAT"
PFEAT = "PFEAT"
HEAD = "HEAD"
PHEAD = "PHEAD"
DEPREL = "DEPREL"
PDEPREL = "PDEPREL"
FILLPRED = "FILLPRED"
PRED = "PRED"
APREDS = "APREDS"

# APRED2 = "APRED2"
# APRED3 = "APRED3"
# APRED1 = "APRED1"
# APRED4 = "APRED4"
# APRED5 = "APRED5"
# APRED6 = "APRED6"
# APRED7 = "APRED7"
# APRED8 = "APRED8"
# APRED9 = "APRED9"

FIXED_COLUMNS = [
    ID,
    FORM,
    LEMMA,
    PLEMMA,
    POS,
    PPOS,
    FEAT,
    PFEAT,
    HEAD,
    PHEAD,
    DEPREL,
    PDEPREL,
    FILLPRED,
    PRED,
]

COLUMNS = FIXED_COLUMNS + [APREDS]
