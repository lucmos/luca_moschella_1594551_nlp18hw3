
import tensorflow as tf
import numpy as np
import tqdm
import time

import utils
import plotter
from chronometer import Chrono
from srl_data_loader import Loader
from srl_data_provider import Provider
from srl_data_batcher import Batcher
from srl_constants import *
from srl_evaluator import Evaluator
from conll_disambiguator import Disambiguator


SRL_TENSORFLOW_MODEL = "./srl_tensorflow_model/model.ckpt"

# todo: remember to set what you want
RESTORE_MODDEL = True
SAVE_MODEL = True
PERFOM_TRAINING = False

BATCH_SIZE = 32
TRAINING_STEPS = 100000
LEARING_RATE = 0.1

HIDDEN_SIZE = 512
NUMBER_OF_DOMAINS = 34
OUTPUT_SIZE = HIDDEN_SIZE*2 + NUMBER_OF_DOMAINS

PRINT_STEPS = 1000

TIME_ID = time.strftime("%d-%m-%Y_%Hh-%Mm")


class SRL:
    """
    Builds the bi-lstm, performs the training and predicts on the DEV and TEST
    """

    def __init__(self):
        self.loader = Loader.get_instance()
        self.provider = Provider.get_instance()

        self.batcher = Batcher(TRAIN)
        self.batcher_dev = Batcher(DEV)
        self.batcher_test = Batcher(TEST)
        self.batcher_dict = {TRAIN: self.batcher, DEV: self.batcher_dev, TEST: self.batcher_test}

        self.number_of_possible_pos = len(set(self.batcher.provider.global_dictionary[POS].values()))
        self.number_of_labels = len(set(self.batcher.provider.global_dictionary[APREDS].values()))

        self.evaluator = Evaluator()

    def train_and_predict(self):
        with tf.name_scope('embeddings'):
            # Loads the embedddings utilized
            embeddings = tf.Variable(np.asarray(self.batcher.provider.embeddings_used),
                                     trainable=False,
                                     dtype=tf.float32)

        with tf.name_scope('inputs'):
            # Defines the placeholders for the inputs. Each input is a list of sentences.
            # Each word in a sentence can be viewed in different ways (literally, its lemma, its pos, etc.)

            # Placeholder for a list of sentences of synsets (int)
            x_train_synset = tf.placeholder(dtype=tf.int32, shape=[None, None])

            # Placeholder for a list of sentences of words (int)
            x_train_words = tf.placeholder(dtype=tf.int32, shape=[None, None])

            # Placeholder for a list of sentences of lemmas (int)
            x_train_lemmas = tf.placeholder(dtype=tf.int32, shape=[None, None])

            # Placeholder for a list of sentences of pos (int)
            x_train_pos = tf.placeholder(dtype=tf.int32, shape=[None, None])

            # Placeholder for a list of sentences, can be 0 or 1. It is 1 in correspondence of the current predicate
            x_train_indicator = tf.placeholder(dtype=tf.int32, shape=[None, None])

            # The sentences are padded to the longest sentence in the batch.
            # This list of integers describes the original length of each sentence.
            x_train_sequence_lenghts = tf.placeholder(dtype=tf.int32, shape=[None])

            # Placeholder for a list of probability distributions that represents the classification
            # of each sentences into the possible domains
            x_train_probas = tf.placeholder(dtype=tf.float32, shape=[None, NUMBER_OF_DOMAINS])

        with tf.name_scope('ground_truth'):
            y_train_labels = tf.placeholder(dtype=tf.int32, shape=(None, None))

        with tf.name_scope('preprocessing'):
            # These variables contain the embeddings looked up
            synset_looked_up_embeddings = tf.nn.embedding_lookup(embeddings, x_train_synset)
            word_looked_up_embeddings = tf.nn.embedding_lookup(embeddings, x_train_words)
            # lemma_looked_up_embedidngs = tf.nn.embedding_lookup(embeddings, x_train_lemmas)

            pos_one_hot = tf.one_hot(x_train_pos, self.number_of_possible_pos, axis=2)
            indicator_one_hot = tf.one_hot(x_train_indicator, 2, axis=-1)

            preprocessed_input = tf.concat([synset_looked_up_embeddings,
                                            word_looked_up_embeddings,
                                            # lemma_looked_up_embedidngs,
                                            pos_one_hot,
                                            indicator_one_hot],
                                           axis=-1)

            # preprocessed_input = word_looked_up_embeddings

        with tf.name_scope("lstm"):
            # The forward and backward cells of the lstm
            cell_fw = tf.contrib.rnn.LSTMCell(HIDDEN_SIZE)
            cell_bw = tf.contrib.rnn.LSTMCell(HIDDEN_SIZE)

            # Performs the computation
            (output_fw, output_bw), _ = tf.nn.bidirectional_dynamic_rnn(cell_fw,
                                                                        cell_bw,
                                                                        preprocessed_input,
                                                                        sequence_length=x_train_sequence_lenghts,
                                                                        dtype=tf.float32,
                                                                        parallel_iterations=64)

            # Concatenates the forward and backward outputs
            context_rep = tf.concat([output_fw, output_bw], axis=-1)

        with tf.name_scope("attention"):
            input_shape = tf.shape(context_rep)
            ntime_steps = input_shape[1]

            att_output = tf.tile(x_train_probas, [1, ntime_steps])
            att_output = tf.reshape(att_output, (-1, ntime_steps, NUMBER_OF_DOMAINS))
            context_rep = tf.concat([att_output, context_rep], -1)

        with tf.name_scope('decoding'):
            W = tf.get_variable("W", shape=[OUTPUT_SIZE, self.number_of_labels], dtype=tf.float32)
            b = tf.get_variable("b", shape=[self.number_of_labels], dtype=tf.float32, initializer=tf.zeros_initializer())

            context_rep_flat = tf.reshape(context_rep, [-1, OUTPUT_SIZE])
            pred = tf.matmul(context_rep_flat, W) + b

            scores = tf.reshape(pred, [-1, ntime_steps, self.number_of_labels])

        with tf.name_scope('predicting'):
            labels_pred = tf.cast(tf.argmax(scores, axis=-1), tf.int32)

        with tf.name_scope('training'):
            losses = tf.nn.sparse_softmax_cross_entropy_with_logits(logits=scores, labels=y_train_labels)

            mask = tf.sequence_mask(x_train_sequence_lenghts)
            losses = tf.boolean_mask(losses, mask)

            tf_loss = tf.reduce_mean(losses)

            optimizer = tf.train.AdagradOptimizer(LEARING_RATE)
            train_op = optimizer.minimize(tf_loss)

        init = tf.global_variables_initializer()
        saver = tf.train.Saver()
        tf.get_default_graph().finalize()

        with tf.Session() as sess:
            if RESTORE_MODDEL:
                saver.restore(sess, SRL_TENSORFLOW_MODEL)
            else:
                sess.run(init)

            if PERFOM_TRAINING:
                avg_loss = 0
                for step in tqdm.tqdm(range(TRAINING_STEPS)):
                    _, b_syn_id, b_probas_id, b_word_id, b_lemmas_id, b_pos_id, b_indicator, b_seq_len, b_labels, _ = self.batcher.next_batch(BATCH_SIZE)

                    _, loss = sess.run([train_op, tf_loss], feed_dict={
                        x_train_synset: b_syn_id,
                        x_train_probas: b_probas_id,
                        x_train_words: b_word_id,
                        x_train_lemmas: b_lemmas_id,
                        x_train_pos: b_pos_id,
                        x_train_indicator: b_indicator,
                        x_train_sequence_lenghts: b_seq_len,
                        y_train_labels: b_labels
                    })

                    avg_loss += loss
                    if not step % PRINT_STEPS:
                        predictions = self.get_predictions(sess, DEV, labels_pred, x_train_synset, x_train_probas, x_train_words, x_train_lemmas, x_train_pos, x_train_indicator, x_train_sequence_lenghts)
                        self.evaluator.eval(predictions)
                        print("current loss:", loss)
                        print("loss media:", avg_loss / step)
                        print()

            predictions = self.get_predictions(sess, DEV, labels_pred, x_train_synset, x_train_probas, x_train_words, x_train_lemmas, x_train_pos, x_train_indicator, x_train_sequence_lenghts)
            self.evaluator.eval(predictions)

            confusion, labels = self.evaluator.generate_confusion_matrix(predictions)
            plotter.plot_confusion_matrix(labels, confusion)

            test_predictions = self.get_predictions(sess, TEST, labels_pred, x_train_synset, x_train_probas, x_train_words, x_train_lemmas, x_train_pos, x_train_indicator, x_train_sequence_lenghts)
            self.save_test_predictions(test_predictions)

            if SAVE_MODEL:
                saver.save(sess, SRL_TENSORFLOW_MODEL)

    def save_test_predictions(self, predictions):
        """
        Saves the test_predictions into a file
        :param predictions: the predictions to be saved
        """
        text = ""
        for i_sen, sentence in enumerate(self.loader.data[TEST][ID]):
            for i_tok, tok in enumerate(sentence):
                fixed_part = [self.provider.decode_representation(self.loader.data[TEST][x][i_sen][i_tok]) for x in FIXED_COLUMNS]
                apreds = self.provider.decode_predictions(predictions[(2, i_sen, i_tok)]) if (2, i_sen, i_tok) in predictions else []
                text += "\t".join(fixed_part + apreds) + "\n"
            text += "\n"

        name = TEST_RESULTS.format(TIME_ID)
        utils.save_string(text, name)
        print("Test predictions generated: {}".format(name))

    @staticmethod
    def add_prediction(predictions, i_dat, i_sen, i_tok, i_pred, label):
        """
        Adds the current prediction into a dictionary, such that the original data format is maintained.
        :param predictions: the dictionary of the predictions
        :param i_dat: the current dataset index
        :param i_sen: the current sentence index
        :param i_tok: the current token index
        :param i_pred: the predicate index relative to the current sentence
        :param label: the predicted label for this instance
        """
        entry = (i_dat, i_sen, i_tok)
        if entry not in predictions:
            predictions[entry] = [label]
        else:
            assert len(predictions[entry]) <= i_pred, "{}\t{}\t{}\t{}\t{}\t".format(i_dat,i_sen,i_tok,i_pred,label)
            predictions[entry].append(label)
        assert predictions[entry][i_pred] == label

    def get_predictions(self, sess, dataset, labels_pred, x_train_synset, x_train_probas, x_train_words, x_train_lemmas, x_train_pos, x_train_indicator, x_train_sequence_lenghts):
        """
        Runs the model and obtains the predictions
        :param sess: the current session of tensorflow
        :param dataset: the dataset to work on
        :param labels_pred: the tensorflow op that contains the predictions
        :param x_train_synset: the x train for the synsets in the sentence
        :param x_train_probas: the x train for the probability distribution of each sentence
        :param x_train_words: the x train for the words in the sentence
        :param x_train_lemmas: the x train for the lemmas in the sentence
        :param x_train_pos:  the x train for the pos in the sentence
        :param x_train_indicator: the x train for the predicate's indicator
        :param x_train_sequence_lenghts: the x train for the length of each sentence
        :return: the computed predictions
        """
        total_preds = []
        total_ids = []
        total_seq_len = []
        predictions = {}
        i = 0

        while True:
            print("Integrity check: must be printed once")
            i += 1
            ids, b_syn_id, b_probas_id, b_word_id, b_lemmas_id, b_pos_id, b_indicator, b_seq_len, _, early_stop = self.batcher_dict[dataset].next_batch(self.batcher_dict[dataset].predicates_len, early_stop=True, ignore_y_train=True)

            preds, = sess.run([labels_pred], feed_dict={
                    x_train_synset: b_syn_id,
                    x_train_probas: b_probas_id,
                    x_train_words: b_word_id,
                    x_train_lemmas: b_lemmas_id,
                    x_train_pos: b_pos_id,
                    x_train_indicator: b_indicator,
                    x_train_sequence_lenghts: b_seq_len,
                })

            total_preds += list(preds)
            total_ids += ids
            total_seq_len += b_seq_len

            if early_stop:
                break

        for (i_dat, i_sen, _, i_pred), sentence, sentence_len in zip(total_ids, total_preds, total_seq_len):
            for i_token, label in enumerate(sentence):
                if i_token >= sentence_len:
                    break
                self.add_prediction(predictions, i_dat, i_sen, i_token, i_pred, label)

        return predictions


if __name__ == '__main__':
    SRL().train_and_predict()

