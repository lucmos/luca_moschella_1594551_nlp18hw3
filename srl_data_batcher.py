import numbers

from chronometer import Chrono
from srl_data_loader import Loader
from srl_data_provider import Provider
from srl_constants import *
from conll_disambiguator import Disambiguator


class Batcher:
    """
    It generates the batches of data.
    """

    def __init__(self, dataset):
        c = Chrono("Generating the batcher {}...".format(dataset))
        self.dataset = dataset
        self.i_dataset = DATASETS.index(dataset)

        self.loader = Loader.get_instance()
        self.provider = Provider.get_instance()
        self.disambiguator = Disambiguator.get_instance()

        self.predicates = self.provider.predicate_positions[self.i_dataset]
        self.predicates_len = len(self.predicates)
        self.index = 0
        c.millis()

    def next_batch(self, batch_size, early_stop=False, ignore_y_train=False):
        """
        Generates the batch.
        It uses the pre computed predicate positions in order to avoid useless lookup.

        :param batch_size: the size of the batch
        :param early_stop: if true, it terminates at the end of the data, regardless of the batch size
        :return: the batch generated
        """
        early_stopped = False
        to_add = []

        # precompute the predicates to use
        while len(to_add) < batch_size:
            to_add.append(self.predicates[self.index])

            self.index += 1
            if self.index == self.predicates_len:
                print("ALL {} THE DATA HAS BEEN USED!".format(self.dataset))
                self.index = 0
                if early_stop:
                    early_stopped = True
                    break

        # utilize the precomputed predicate to create the batch
        max_len = max(to_add, key=lambda x: self.provider.sentences_len[x[0]][x[1]])
        max_len = self.provider.sentences_len[max_len[0]][max_len[1]]

        # pre allocate arrays to avoid repeated resize
        dinamyc_batch_len = len(to_add)
        zero_sentence = [0] * max_len
        synsets_id = [zero_sentence[:] for _ in range(dinamyc_batch_len)]
        words_id = [zero_sentence[:] for _ in range(dinamyc_batch_len)]
        lemmas_id = [zero_sentence[:] for _ in range(dinamyc_batch_len)]
        pos_id = [zero_sentence[:] for _ in range(dinamyc_batch_len)]
        preds_indicator = [zero_sentence[:] for _ in range(dinamyc_batch_len)]

        sentence_len = [0] * dinamyc_batch_len
        probas = [None] * dinamyc_batch_len

        y_train = [zero_sentence[:] for _ in range(dinamyc_batch_len)] if not ignore_y_train else [[] for _ in range(dinamyc_batch_len)]

        # avoid repeated lookup
        mapping_word_syn = self.provider.word_and_synset_dictionary
        global_dictionary = self.provider.global_dictionary

        # fill the pre allocated arrays
        for batch_index, (i_dat, i_sen, i_tok, i_pred) in enumerate(to_add):
            sen_len = self.provider.sentences_len[i_dat][i_sen]
            sentence_len[batch_index] = sen_len

            proba_from_sentence_in_conll = self.disambiguator.sentences_proba_conll[DATASETS[i_dat]][i_sen]
            probas[batch_index] = proba_from_sentence_in_conll

            # avoid repeated lookup
            synset_from_sentence_in_conll = self.disambiguator.disambiguated_conll[DATASETS[i_dat]][i_sen]

            word_from_sentence_in_data = self.loader.data[DATASETS[i_dat]][FORM][i_sen]
            lemma_from_sentence_in_data = self.loader.data[DATASETS[i_dat]][LEMMA][i_sen]
            pos_from_sentence_in_data = self.loader.data[DATASETS[i_dat]][POS][i_sen]
            lables_from_sentence_in_data = self.loader.data[DATASETS[i_dat]][APREDS][i_sen]

            for iw in range(sen_len):
                word = word_from_sentence_in_data[iw]
                words_id[batch_index][iw] = mapping_word_syn[word]

                lemma = lemma_from_sentence_in_data[iw]
                lemmas_id[batch_index][iw] = mapping_word_syn[lemma]

                synset = synset_from_sentence_in_conll[iw]
                synsets_id[batch_index][iw] = mapping_word_syn[synset] if synset else mapping_word_syn[word]

                pos = pos_from_sentence_in_data[iw]
                pos_id[batch_index][iw] = global_dictionary[POS][pos]

                preds_indicator[batch_index][iw] = int(iw == i_tok)

                if not ignore_y_train:
                    label = lables_from_sentence_in_data[iw][i_pred]
                    y_train[batch_index][iw] = global_dictionary[APREDS][label]

        assert len(to_add) == len(words_id) == len(pos_id) == len(preds_indicator) == len(sentence_len)
        return to_add, synsets_id, probas, words_id, lemmas_id, pos_id, preds_indicator, sentence_len, y_train, early_stopped


if __name__ == '__main__':
    b = Batcher(DEV)

    _, syn_list, probas, w_list, lemma_list, pos_lsit, indic, _, y_list, _ = b.next_batch(64)

    print("{}\t{}\t{}\t{}\t{}".format("WORD".ljust(18), "LEMMA".ljust(18), "POS".ljust(18), "PRED?".ljust(18), "LABEL"))
    print("-"*(18*5))
    for sen_syn, probs, sen_word, sen_lemma, sen_pos, sen_ind, sen_y in zip(syn_list, probas, w_list, lemma_list, pos_lsit, indic, y_list):
        for syn, word, lemma, pos, ind, y in zip([b.provider.reverse_word_and_synset_dictionary[x] for x in sen_syn],
                                            [b.provider.reverse_word_and_synset_dictionary[x] for x in sen_word],
                                            [b.provider.reverse_word_and_synset_dictionary[x] for x in sen_lemma],
                                            [b.provider.global_reverse_dictionary[POS][x] for x in sen_pos],
                                            [x for x in sen_ind],
                                            [b.provider.global_reverse_dictionary[APREDS][x] for x in sen_y]):
            print("{}\t{}\t{}\t{}\t{}\t{}\t\t{}".format(syn.ljust(18), word.ljust(18), lemma.ljust(18), pos.ljust(18), str(ind).ljust(18), y, list(probs)))
        print()
        print()
    assert False

    import tqdm

    b = Batcher(a)
    _, syn_list, w_list, lemma_list, pos_lsit, indic, sen_len, y_list, _ = b.next_batch(b.predicates_len, ignore_y_train=True)

    for i in tqdm.tqdm(range(1000000)):
        _, syn_list, w_list, lemma_list, pos_lsit, indic, sen_len, y_list, _ = b.next_batch(64)
