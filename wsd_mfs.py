from wsd_constants import BABELNET, WORDNET, ALL_BABELNET, DEV
from wsd_loader import Loader, TRAIN, ID, WORD, LEMMA, POS
from babelnet_importer import BabelMap


class MFS:
    """
    Manages the most-frequent sense predictions, for the words not present in the train set
    """

    def __init__(self):
        self.loader = Loader.get_instance()
        self.babelmap = BabelMap.get_instance()

    def with_word(self, word, _, __):
        """
        This function must be used as argument of the predictor.
        Indicates that the prediction must be performed according to the word
        """
        return word, self.loader.word2synsets[TRAIN]

    def with_lemma(self, _, lemma, __):
        """
        This function must be used as argument of the predictor.
        Indicates that the prediction must be performed according to the lemma
        """
        return lemma, self.loader.lemma2synsets[TRAIN]

    def with_wordpos(self, word, _, pos):
        """
        This function must be used as argument of the predictor.
        Indicates that the prediction must be performed according to the (word, pos)
        """
        return (word, pos), self.loader.word_pos2synsets[TRAIN]

    def with_lemmapos(self, _, lemma, pos):
        """
        This function must be used as argument of the predictor.
        Indicates that the prediction must be performed according to the (lemma, pos)
        """
        return (lemma, pos), self.loader.lemma_pos2synsets[TRAIN]

    def predictor_template(self, predictor_types, kbs):
        """
        This function builds a predictor, that will act according to the parameters given.
        predictor_types[i] indicates on what basis the prediction made by kbs[i] must be done.
        If kbs[i] is None, it will be used the train data.
        :param predictor_types: defines on which feature base the prediction.
                                Available values for predictor_type are the functions
                                defined in this class: word or lemma, with or without pos

        :param kbs: defines on what data base the predictions
                    Available values for kbs are: None, WORDNET, BABELNET, ALL_BABELNET (see constants)

        :return: a predictor function.
        """
        def _predictor(word, lemma, pos):
            word2common = self.babelmap.word2most_common
            for (token, dictionary), kb in zip((x(word, lemma, pos) for x in predictor_types), kbs):
                if not kb:
                    if token in dictionary:
                        commons = dictionary[token].most_common(2)
                        if len(commons) == 1 or commons[0][1] != commons[1][1]:
                            return commons[0][0]
                else:
                    if isinstance(token, tuple):
                        token, _ = token
                    if token in word2common and kb in word2common[token] and pos in word2common[token][kb]:
                        return word2common[token][kb][pos]
                    else:
                        if kb == ALL_BABELNET:
                            if token in word2common and kb in word2common[token] and "ALL" in word2common[token][kb]:
                                return word2common[token][kb]["ALL"]
                            else:
                                pass  # todo: introduces silent ignore
                                # print("WARNING: not presented in extracted", kb + ":", token + " - " + pos)
            return None
        return _predictor

    def baseline(self, predictor, dataset_name=DEV):
        """
        Predicts all the ambiguous words in dataset according to the predictor function

        :param predictor: the predictor function to use
        :param dataset_name: the dataset to be predicted
        :return: the predictions performed
        """
        predictions = {}
        for d, document in enumerate(self.loader.doc_sentences[dataset_name][ID]):
            for s, sentence in enumerate(document):
                for i, idd in enumerate(sentence):
                    if not idd:
                        continue

                    word = self.loader.doc_sentences[dataset_name][WORD][d][s][i]
                    lemma = self.loader.doc_sentences[dataset_name][LEMMA][d][s][i]
                    pos = self.loader.doc_sentences[dataset_name][POS][d][s][i]

                    pred = predictor(word, lemma, pos)
                    if pred:
                        predictions[idd] = pred
        return predictions

    def baseline_monosemous(self, dataset_name=DEV):
        """
        Predicts all the monosemous words in dataset
        :param dataset_name: the dataset to be predicted
        :return: the predictions performed
        """
        predictions = {}
        predictor = self.predictor()
        for i_doc, doc in enumerate(self.loader.doc_sentences[dataset_name][ID]):
            for i_sen, sentence in enumerate(self.loader.doc_sentences[dataset_name][ID][i_doc]):
                for i_id, idd in enumerate(self.loader.doc_sentences[dataset_name][ID][i_doc][i_sen]):
                    if idd:

                        pos = self.loader.doc_sentences[dataset_name][POS][i_doc][i_sen][i_id]
                        word = self.loader.doc_sentences[dataset_name][WORD][i_doc][i_sen][i_id]
                        lemma = self.loader.doc_sentences[dataset_name][LEMMA][i_doc][i_sen][i_id]

                        entry = (word, pos)
                        if entry in self.loader.word_pos2synsets[TRAIN] and len(self.loader.word_pos2synsets[TRAIN][entry]) == 1:
                            pred = predictor(word, lemma, pos)
                            if pred:
                                predictions[idd] = pred
        return predictions

    def autocompolete_predictios(self, ids, dataset_name=DEV):
        """
        Checks if the some predictions are missing from ids, predicts the correspondent synset and returns those predictions.
        :param ids: the ID of the predictions already performed
        :param dataset_name: the dataset on which check the predictions
        :return: the missing predictions
        """

        ids = set(ids)
        autocomplete_predictions = {}
        predictor = self.predictor()
        for i_doc, doc in enumerate(self.loader.doc_sentences[dataset_name][ID]):
            for i_sen, sentence in enumerate(self.loader.doc_sentences[dataset_name][ID][i_doc]):
                for i_id, idd in enumerate(self.loader.doc_sentences[dataset_name][ID][i_doc][i_sen]):
                    if idd and idd not in ids:

                        pos = self.loader.doc_sentences[dataset_name][POS][i_doc][i_sen][i_id]
                        word = self.loader.doc_sentences[dataset_name][WORD][i_doc][i_sen][i_id]
                        lemma = self.loader.doc_sentences[dataset_name][LEMMA][i_doc][i_sen][i_id]
                        pred = predictor(word, lemma, pos)
                        if pred:
                            autocomplete_predictions[idd] = pred
        return autocomplete_predictions

    def predictor(self):
        """
        Return the default predictor to use
        :return: the predictor
        """
        return self.predictor_template([self.with_wordpos, self.with_lemma, self.with_lemma, self.with_lemma],
                                       [None, WORDNET, BABELNET, ALL_BABELNET])


